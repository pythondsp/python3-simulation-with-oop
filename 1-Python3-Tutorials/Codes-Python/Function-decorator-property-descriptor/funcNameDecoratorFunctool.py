# funcNameDecoratorFunctool.py
from functools import wraps

def funcNameDecoratorFunctool(func): # function as input
	#func is the function to be wrapped
	@wraps(func) 
	def printFuncName(*args, **kwargs): #take all arguments of function as input
		print("Function Name:", func.__name__) # print function name
		return func(*args, **kwargs) # return function with all arguments
	return printFuncName

