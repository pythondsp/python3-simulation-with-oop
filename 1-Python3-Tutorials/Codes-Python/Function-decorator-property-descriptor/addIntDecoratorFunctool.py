#addIntDecoratorFunctool.py
from funcNameDecoratorFunctool import funcNameDecoratorFunctool 

@funcNameDecoratorFunctool
def addIntDecorator(x:int, *,  y:int) -> int:
	'''add two variables (x, y):
		x: integer, postional argument
		y: integer, keyword argument
		returnType: integer
	'''
	return (x+y)

intAdd=addIntDecorator(2, y=3) # Function Name: addIntDecorator
print("intAdd =", intAdd) # 5

help(addIntDecorator) # lines 6-9 will be displaed

print(addIntDecorator.__annotations__) 
##{'return': <class 'int'>, 'y': <class 'int'>, 'x': <class 'int'>} 

