#rectGeneralized.py
class TypeCheck: 
	def __init__(self, parameter, expected_type):
		self.parameter = parameter
		self.expected_type = expected_type

	def __get__(self, instance, cls):
		if instance is None: # required if descriptor is  
			return self # used as class variable
		else: # in this code, only following line is required
			return instance.__dict__[self.parameter]

	def __set__(self, instance, value):
		print("setting %s to %s" % (self.parameter, value))
		if not isinstance(value, self.expected_type):
			raise TypeError("%s value is expected" % self.expected_type)
		instance.__dict__[self.parameter] = value

def typeAssert(**kwargs):
	def decorate(cls):
		for parameter, expected_type in kwargs.items():
			setattr(cls, parameter, TypeCheck(parameter, expected_type))
		return cls
	return decorate

 # define attribute types here in the decorator
@typeAssert(author=str, length = int, width = float)
class Rect:
	def __init__(self, *,  length, width, author = ""): #require kwargs
		self.length = length
		self.width = width * 1.0 # to accept integer as well
		self.author = author

r = Rect (length=3, width=3.1, author = "Meher")
## setting length to 3
## setting width to 3.1
## setting author to Meher

#r = Rect (length="len", width=3.1, author = "Meher") # error shown below
## File "rectProperty.py", line 42,
## [ ... ] 
## TypeError: <class 'int'> value is expected