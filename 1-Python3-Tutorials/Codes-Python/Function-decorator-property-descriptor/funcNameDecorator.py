# funcNameDecorator.py
def funcNameDecorator(func): # function as input
	def printFuncName(*args, **kwargs): #take all arguments of function as input
		print("Function Name:", func.__name__) # print function name
		return func(*args, **kwargs) # return function with all arguments
	return printFuncName