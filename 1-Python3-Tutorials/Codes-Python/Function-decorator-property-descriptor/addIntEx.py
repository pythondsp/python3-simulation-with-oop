#addIntEx.py

# line 6-9 will be displayed, 
# when help command is used for addIntEx as shown in line 13.
def addIntEx(x,y):
	'''add two variables (x, y):
		x: integer
		y: integer
		returnType: integer
	'''
	return (x+y)

help(addIntEx)  # line 6-9 will be displayed as output

#adding numbers: desired result
intAdd = addIntEx(2,3)
print("intAdd =", intAdd) # 5

# adding strings: undesired result
# attribute validation is used for avoiding such errors
strAdd = addIntEx("Meher ", "Krishna") 
print("strAdd =", strAdd) # Meher Krishna

