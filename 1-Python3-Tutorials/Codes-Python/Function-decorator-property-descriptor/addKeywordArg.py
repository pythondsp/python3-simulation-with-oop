#addKeywordArg.py
def addKeywordArg(x:int, *,  y:int) -> int:
	'''add two numbers:
         x: integer, postional argument
         y: integer, keyword argument
         returnType: integer	'''
	return (x+y)

Add1 = addKeywordArg(2, y=3) # x: positional arg and y: keyword arg
print(Add1) # 5

Add2 = addKeywordArg(y=3, x=2) # x and y as keyword argument
print(Add2) # 5

## it's wrong, because y is not defined as keyword argument
#Add3 = addPositionalArg(2, 3) # y should be keyword argument i.e. y=3

## keyword arg should come after positional arg
#Add4 = addPositionalArg(y=3, 2) # correct (2, y=3)

help(addKeywordArg) # Lines 3-6 will be displayed as output

print(addKeywordArg.__annotations__)
## {'return': <class 'int'>, 'x': <class 'int'>, 'y': <class 'int'>}

## line 2 is only help (not validation), i.e. string addition will still unchecked
strAdd = addKeywordArg("Meher ", y = "Krishna")
print("strAdd =", strAdd)