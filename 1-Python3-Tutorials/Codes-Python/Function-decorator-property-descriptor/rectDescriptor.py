#rectDescriptor.py
class Integer: 
	def __init__(self, parameter):
		self.parameter = parameter

	def __get__(self, instance, cls):
		if instance is None: # required if descriptor is  
			return self # used as class variable
		else: # in this code, only following line is required
			return instance.__dict__[self.parameter]

	def __set__(self, instance, value):
		print("setting %s to %s" % (self.parameter, value))
		if not isinstance(value, int):
			raise TypeError("Interger value is expected")
		instance.__dict__[self.parameter] = value

class Rect: 
	length = Integer('length')
	width = Integer('width')
	def __init__(self, length, width):
		self.length = length
		self.width = width
  
	def area(self):
		'''Calculates Area: length*width'''
		return self.length * self.width

r = Rect(3,2)
## setting length to 3
## setting width to 3

print(r.length) # 3

print("Area:", Rect.area(r)) # Area:  6

#r = Rect(3, 1.5)
## TypeError: Interger value is expected