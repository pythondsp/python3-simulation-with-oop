#addIntDecorator.py
from funcNameDecorator import funcNameDecorator 

@funcNameDecorator
def addIntDecorator(x:int, *,  y:int) -> int:
	'''add two variables (x, y):
		x: integer, postional argument
		y: integer, keyword argument
		returnType: integer
	'''
	return (x+y)

## decorator will be executed when function is called,
## and function name will be displayed as output as shown below,
intAdd=addIntDecorator(2, y=3) # Function Name: addIntDecorator
print("intAdd =", intAdd) # 5

##problem with decorator: help features are not displayed as shown below
help(addIntDecorator) # following are the outputs of help command
## Help on function wrapper in module funcNameDecorator:
## wrapper(*args, **kwargs)

## problem with decorator: no output is displayed
print(addIntDecorator.__annotations__) # {} 

