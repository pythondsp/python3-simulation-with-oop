#addIntValidation.py
def addIntValidation(x:int, *,  y:int)->int:
	'''add two variables (x, y):
		x: integer, postional argument
		y: integer, keyword argument
		returnType: integer
		'''
	if type(x) is not int: # validate input 'x' as integer type
		raise TypeError("Please enter integer value for x")

	if type(y) is not int: # validate input 'y' as integer type
		raise TypeError("Please enter integer value for y")
  
	return (x+y)

intAdd=addIntValidation(y=3, x=2)
print("intAdd =", intAdd)

#strAdd=addIntValidation("Meher ", y = "Krishna")
## Following error will be generated for above command, 
## raise TypeError("Please enter integer value for x")
## TypeError: Please enter integer value for x

help(addIntValidation) # Lines 3-6 will be displayed as output 
print(addIntValidation.__annotations__)
## {'return': <class 'int'>, 'x': <class 'int'>, 'y': <class 'int'>}