#rectProperty.py
class Rectangle:
	'''
	-Calculate Area and Perimeter of Rectangle
	-getter and setter are used to displaying and setting the length value. 
	-width is set by init function
	'''

	# self.length is used in below lines, 
	# but length is not initialized by __init__, 
	# initialization is done by .setter at lines 34 due to @property at line 27,
	# also value is displayed by getter (@propety) at line 28-32
	# whereas `width' is get and set as simple python code and without validation
	def __init__(self, length, width):
		#if self._length is used, then it will not validate through setter.
		self.length = length  
		self.width = width

	@property
	def area(self):
		'''Calculates Area: length*width'''
		return self.length * self.width
 
	def perimeter(self):
		'''Calculates Perimeter: 2*(length+width)'''
		return 2 * (self.length + self.width)

	@property
	def length(self):
		'''displaying length'''
		print("getting length value through getter")
		return self._length

	@length.setter
	def length(self, value):
		'''setting length'''
		print("saving value through setter", value)
		if not isinstance(value, int): # validating length as integer
			raise TypeError("Only integers are allowed")
		self._length = value

r = Rectangle(3,2) # following output will be displayed
## saving value through setter 3
print(r.length) # following output will be displayed
## getting length value through getter
## 3

## @property is used for area, 
## therefore it can be accessed directly to display the area
print(r.area) # following output will be displayed
## getting length value through getter
## 6

#r=Rectangle(4.3, 4) # following error will be generated
## [...]
## TypeError: Only integers are allowed

# print perimeter of rectangle
print(Rectangle.perimeter(r)) 
## getting length value through getter
## 10