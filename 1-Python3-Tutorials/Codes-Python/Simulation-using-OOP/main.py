# main.py
import matplotlib.pyplot as plt
import numpy as np

from BPSK import BPSK

def main():
  bit_length  = 10000
  iter_len=30

  #noise calculations
  #SNR db range: -2 to 10
  SNR_db = np.array(np.arange(-2, 10, 1),float)
  SNR = np.zeros(len(SNR_db), float)
  for i in range (len(SNR)):
    SNR[i]= 1/np.sqrt(2)*10**(-SNR_db[i]/20)

  #instance of BPSK class
  bpsk = BPSK(bit_length)

  #accumulate bit error rate for various iterations
  instantaneous_error =np.zeros((iter_len, len(SNR_db)), float)
  for iter in range(iter_len):
    error_matrix =np.zeros(len(SNR_db), float)

    for i in range(len(SNR)):
      signal =  bpsk.signal_generator()
      noise = bpsk.noise_generator()
      recieved_signal = bpsk.recieved_signal(signal, SNR[i], noise)
      detected_signal = bpsk.detected_signal(recieved_signal)
      error = bpsk.error(signal, detected_signal)
      error_matrix[i] = error

    instantaneous_error[iter]=error_matrix

  #Average BER
  BerBPSK = instantaneous_error.sum(axis=0)/(iter_len*bit_length)

  #calculate theoritical BER
  theoryBerBPSK = bpsk.theoryBerBPSK(SNR_db)
  
  #plot data (not from the .csv file)
  plt.semilogy(SNR_db, BerBPSK,'--')
  plt.semilogy(SNR_db, theoryBerBPSK, 'mo')
  plt.ylabel('BER')
  plt.xlabel('SNR')
  plt.title('BPSK BER Curves')
  plt.legend(['Simulation', 'Theory'], loc='upper right')
  plt.grid()
  plt.show()

# Standard boilerplate to call the main() function.
if __name__ == '__main__':
  main()