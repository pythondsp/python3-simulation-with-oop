#decoratorMethod.py
from datetime import datetime

class Date:
	# decorator as instance method
	def instanceMethodDecorator(self, func): #day-month-year
		def wrapper(*args, **kwargs):
			print("instanceMethodDecorator called at time:", 
				datetime.today())
			return func(*args, **kwargs) 
		return wrapper

	# decorator as class method
	@classmethod
	def classMethodDecorator(self, func): #day-month-year
			def wrapper(*args, **kwargs):
				print("classMethodDecorator called at time:", 
					datetime.today())
				return func(*args, **kwargs) 
			return wrapper

a = Date()
@a.instanceMethodDecorator
def add(a,b): #decorated using instanceMethodDecorator
	print(a+b)

@Date.classMethodDecorator
def sub(a,b): #decorated using classMethodDecorator
	print(a-b)

sum = add(3, 2)
# instanceMethodDecorator called at 
# time: 2016-06-02 06:18:14.776555
# 5

sub = sub(3,2)
# classMethodDecorator called at 
# time: 2016-06-02 06:18:14.776555
# 1