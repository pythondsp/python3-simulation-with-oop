# multipleConstructor.py
class Date:
    def __init__(self, year, month, day): #year-month-day
        self.year = year
        self.month = month
        self.day = day
     
    def __str__(self):
        return("Date in dmy format : %s-%s-%s" % (self.day, self.month, self.year))

    @classmethod
    def dmy(cls, day, month, year): #day-month-year
        # print("dmy")
        cls.year = year
        cls.month = month
        cls.day = day
        #order of return should be same as init
        return cls(cls.year, cls.month, cls.day) 

    @classmethod
    def mdy(cls, month, day, year): #month-day-year
        # print("mdy")
        cls.year = year
        cls.month = month
        cls.day = day
        #order of return should be same as init
        return cls(cls.year, cls.month, cls.day)

a=Date(2016, 12, 11)
print(a) # Date in dmy format : 11-12-2016

b=Date.dmy(9, 10, 2015)
print(b) # Date in dmy format : 9-10-2015

c=Date.mdy(7, 8, 2014)
print(c) # Date in dmy format : 8-7-2014