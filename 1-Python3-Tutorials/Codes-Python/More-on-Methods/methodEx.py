#methodEx.py
class Add:
	x = 5
	def __init__(self, x):
		self.x = x

	#simple method
	def addFunc(self, y):
		print ("method:", self.x+y)

	@classmethod
	#as convention, cls must be in place of self
	def addClass(self, y): # i.e. def addClass(cls, y): is the correct way
		print ("class:", self.x+y)

	@staticmethod
	def addStatic(y): #no self is used here
		print("static:", x+y) 

#METHOD
aFunc = Add(4)
aFunc.addFunc(3) # method: 7

#CLASS METHOD
aClass = Add(4)
#below line will not use init function's self.x = 4
#it will use class variable value i.e. x = 5
aClass.addClass(3) # class: 8

#Directly calling @classmethod
Add.addClass(3) # class: 8

#STATIC METHOD
# x is defined here for static method, it will get it from the class
x=12 

aStatic = Add(4)
aStatic.addStatic(3) #static: 15

#Directly calling @staticmethod
Add.addStatic(3) #static: 15