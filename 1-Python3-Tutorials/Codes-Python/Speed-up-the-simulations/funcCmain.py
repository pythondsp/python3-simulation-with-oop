#funcCmain.py

#to import pyx file: following two lines are needed
import pyximport
pyximport.install()

#call functions from files
from funcCfile import add2Num, diff2Num

##to call everything from file, use below lien. 
#from funcFile import *

x = add2Num(2,3)
print(x) # 5 

y = diff2Num(2, 3)
print(y) # -1