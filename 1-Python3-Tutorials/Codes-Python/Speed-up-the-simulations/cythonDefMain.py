#cythonDefMain.py

#to import pyx file: following two lines are needed
import pyximport
pyximport.install()

#call functions from files
from cythonDef import *

print("pyfunc ", pyfunc(2)) # 3

print("cyfunc ", cyfunc(2)) #3

##error: cdef can not be called from outside
#print(cfunc(int x)) 

#cpdef can be called from outside the function file
print("cypyfunc ", cypyfunc(2)) # 3

#cfunc (cdef) is called through cypyfunc2 (cpdef)
print("cypyfunc2 ", cypyfunc2(2)) # 4