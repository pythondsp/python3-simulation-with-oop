#numbaLoop.py

from numba import autojit
N=10000

#pure python function
@autojit
def numbaLoop(x):
    for i in range(N):
        for j in range(N):
            x = x+1
    return(x)