#cythonMain.py

#to import pyx file: following two lines are needed
import pyximport
pyximport.install()

import time
from cythonFunc import * 

start_time = time.time()
print(pyloop(2))
print("--- %s seconds ---" % (time.time() - start_time)) #9.69 sec

start_time = time.time()
print(cyloop(2))
print("--- %s seconds ---" % (time.time() - start_time)) #5.28 sec

start_time = time.time()
print(cypyloop(2))
print("--- %s seconds ---" % (time.time() - start_time)) #5.25 sec

start_time = time.time()
print(cypyloop2(2))
print("--- %s seconds ---" % (time.time() - start_time)) #0.055 sec