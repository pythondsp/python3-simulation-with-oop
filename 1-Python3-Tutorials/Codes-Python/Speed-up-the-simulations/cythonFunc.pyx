#cythonFunc.pyx

N=10000
#pure python function
def pyloop(x):
    for i in range(N):
        for j in range(N):
            x = x+1
    return(x)

# cython function: as input type for argument 'x' is defined
def cyloop(int x):
    for i in range(N):
        for j in range(N):
            x = x+1
    return (x)

# cpdef: it can be called from outside file
# adds python wrapper to code
cpdef int cypyloop(int x):
    for i in range(N):
        for j in range(N):
            x = x+1
    return(x)

# cpdef: it can be called from outside file
cpdef int cypyloop2(int x):
    cdef: #define all variable types inside cdef
        int i, j
    for i in range(N):
        for j in range(N):
            x = x+1
    return(x)