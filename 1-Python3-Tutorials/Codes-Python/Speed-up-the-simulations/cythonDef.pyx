#cythonDef.pyx

#pure python function run by cython
def pyfunc(x):
    return (x+1)

# cython function: as input type for argument 'x' is defined
#def is used to define it. 
def cyfunc(int x):
    return (x+1)

# cdef: return type 'int' is defined, cdef/cpdef is required for this. 
# cdef function can not be called from other file
# it can be called within same file
cdef int cfunc(int x):
    return(x+1)

# cpdef: it adds the python wrapper 
# hence, it can be called from outside file
cpdef int cypyfunc(int x):
    return(x+1)

# cpdef:
cpdef int cypyfunc2(int x):
    y = cfunc(x)
    return(y+1)