#main.py
import sys
import os

a=5
b=2

# 1. 
from add2Num import add2
print(add2(a,b)) # 5

# 2. 
#no need for sys.path for 'multiply folder', 
#as it is available in the main folder
from multiply.xyz.product2Num import product2Num
print(product2Num(a,b)) # 10

# 3. 
#add `multiply2' folder in the path
sys.path.append(os.path.join(os.path.dirname(__file__), 'multiply2'))
from multiply2Num import product2Num
print(product2Num(a,b)) # 10

# 4. 
#add all files and folder from back folder
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
# 'diff2' folder is available at back folder
# folder name 'diff2' is required as it is not explictly imported
from diff2.diff2Num import diff2 
print(diff2(a,b)) # 3

# 5. 
#add only `diff' folder from back folder
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'diff'))
# no need to write diff.diff2Num, because folder is already imported
from diffNum import diff 
print(diff(a,b)) # 3


# 6. 
# add all files and folder from back-back folder to code
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
# 'divide2Num.py' file is available at back-back folder
from divide2Num import divide2 
print(divide2(a,b)) # 2.5






