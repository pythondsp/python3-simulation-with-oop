# specialMatrix.py

import numpy as np

I = np.identity(3) # identity matrix
print(I)
'''
[[ 1.  0.  0.]
 [ 0.  1.  0.]
 [ 0.  0.  1.]]
'''

d = np.diag([2, 3, 4]) # diagonal matrix
print(d)
'''
[[2 0 0]
 [0 3 0]
 [0 0 4]]
'''

z = np.zeros(3) #zero matrix
print(z) # [ 0.  0.  0.]

z32 = np.ones((3,2), dtype=float) # one matrix of size (3,2)
print(z32)
'''
[[ 1.  1.]
 [ 1.  1.]
 [ 1.  1.]]
'''
 
zI = np.zeros_like(I) # define zero matrix of size 'I' at line 5
print(zI) # zero matrix of order (3,3) as I define above

E = np.eye(3) # identity matrix of size (3,3)
print(E) 

dE = np.eye(5, k=2) # identity matrix along k-th diagonal
print(dE)
'''
[[ 0.  0.  1.  0.  0.]
 [ 0.  0.  0.  1.  0.]
 [ 0.  0.  0.  0.  1.]
 [ 0.  0.  0.  0.  0.]
 [ 0.  0.  0.  0.  0.]]
'''