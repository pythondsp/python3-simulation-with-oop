# randomNumberEx.py

import numpy.random as nr
import matplotlib.pyplot as plt

# generate sequence of 0 and 1
rInt = nr.randint(0, 2, 10) 
print(rInt) # [0 1 0 1 1 1 1 1 1 0]

# generate sequence of +/- 1 
rInt2 = [2*i + 1 for i in nr.randint(-1, 1, 10)]
print(rInt2) # [1, -1, 1, 1, 1, -1, 1, -1, -1, -1]

plt.close("all")

# uniform random variable
plt.subplot(3,1,1)
uniformRandom = nr.rand(10000)
plt.hist(uniformRandom, 30, label="Uniform")
plt.legend()

# normal random variable
plt.subplot(3,1,2)
normalRandom = nr.randn(10000)
plt.hist(normalRandom, 25, label="Gaussian")
plt.legend()

# Rayleigh distributed random number
plt.subplot(3,1,3)
rayleighRandom = nr.rayleigh(1, 10000)
plt.hist(rayleighRandom, 30, label="Rayleigh")
plt.legend()

plt.show()