# defineArray.py

import numpy as np 

# one dimensional array
a = np.array([1, 2, 3])
print (a) # [1 2 3]

# two dimensional array
b = np.array([
        [1, 2, 3],
        [4, 5, 6]
    ])
## or define as follows in one line: less readable
# b = np.array([[1, 2, 3],[4, 5, 6]])
print(b)
'''
[1 2 3]
[[1 2 3]
 [4 5 6]]
'''