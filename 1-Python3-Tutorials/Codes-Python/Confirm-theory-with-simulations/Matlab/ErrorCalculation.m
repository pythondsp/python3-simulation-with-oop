% ErrorCalculation.m
function error = ErrorCalculation(bitLength, noiseAmp)
  % uniformly distributed sequence to generate singal
  ip = rand(1,bitLength)>0.5; % generating 0,1 with equal probability
  b = 2*ip-1; % BPSK modulation : convert 0 to -1; 1 to 1

  %binary signal generated from 'b'
  signal = zeros(1, bitLength);
  for i=1:length(b)
    if b(i) < 0
      signal(i)=-1;
    else
      signal(i)=1;
    end
  end
  %Normal Distribution: Gaussian Noise with unit variance
  noise = randn(1, bitLength);

  %recevied signal
  rxSignal = signal + noiseAmp*noise;
  
  % decision:
  detectedSignal = zeros(1, (bitLength));
  for i = 1:length(b)
    if rxSignal(i) < 0
      detectedSignal(i)=-1;
    else
      detectedSignal(i)=1;
    end
  end

  %error matrix: save 1 if error else 0
  errorMatrix = abs((detectedSignal - signal)/2);
  error=sum(errorMatrix); %add all 1's for total error