%SimBPSK.m
clear all
close all
clc

tic
bitLength  = 100000; %  100000; %number of transmitted bit
iterLen=30; %iterate 30 time for better average

SNRdB = -2:1:10;
SNR = 10.^(SNRdB/10); % SNR_db to SNR conversion
noise = zeros(1,length(SNRdB));

error =zeros(iterLen, length(SNRdB));

for iL = 1:iterLen
  %Generate various Noise-voltage (N0/2) Level
  for i =1:length(noise)
    noise(1,i) = 1/sqrt(2*SNR(i)); % N0/2 = 1/sqrt(2*noisePower)
  end

  %calculate error
  errorMatrix =zeros(1, length(SNRdB));
  for i=1:length(noise)
    errorMatrix(i) = ErrorCalculation(bitLength, noise(i));    
  end
  %save error at each iteration
  error(iL,:)=errorMatrix;
end

%average error
BER = sum(error)/(iterLen*bitLength);

%theoritical error
theoryBER = zeros(1, length(SNRdB));
for i= 1:length(SNRdB)
  theoryBER(i) = 0.5*erfc(sqrt(SNR(i)));
end

toc1 = toc;
%print simulation time
fprintf('elapse time: %0.5f sec', toc1) %    sec

%plot the graph
semilogy(SNRdB, BER,'--r')
hold on
semilogy(SNRdB, theoryBER, '*')
legend('Simulation', 'Theory')