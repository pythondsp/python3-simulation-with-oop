#theoryBPSK.py: plot theoretical BER of BPSK

import matplotlib.pyplot as plt
import numpy as np
from scipy.special import erfc 


def main():
	#BPSK: theoretical error
	SNRdB = np.array(np.arange(-2, 10, 1)) #SNR in db
	SNR = 10**(SNRdB/10) # SNRdB to SNR conversion

	theoryBER = np.zeros(len(SNRdB),float) # zero vector of size SNRdB

	for i in range(len(SNRdB)):
	    theoryBER[i] = 0.5*erfc(np.sqrt(SNR[i]))

	#plot results
	plt.semilogy(SNRdB, theoryBER, '--mo') 
	plt.ylabel('BER')
	plt.xlabel('SNR')
	plt.title('BPSK BER Curves')
	plt.legend(['Theory'])
	plt.grid()
	plt.show()

# Standard boilerplate to call the main() function.
if __name__ == '__main__':
  main()