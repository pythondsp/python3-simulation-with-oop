# arrayMatDiff.py

import numpy as np

# type difference
m = np.mat('1, 1, 1; 2, 4, 2; 3, 3, 5')    
print(type(m)) # numpy.matrixlib.defmatrix.matrix

n = np.array([[1, 1, 1], [2, 4, 2 ], [3, 3, 5]])
print(type(n)) # numpy.ndarray

# multiplication
print(m*m*m) # matrix multiplication
'''
[[ 46  62  62]
 [124 172 164]
 [186 246 254]]
'''

print(n*n*n) # point to point multiplication
'''
[[  1   1   1]
 [  8  64   8]
 [ 27  27 125]]
 '''
 
#To perform matric multiplicaiton using array, 
# we need to use 'dot' operator, 
print(n.dot(n).dot(n)) # matrix multiplication using array
'''
[[ 46  62  62]
 [124 172 164]
 [186 246 254]]
 '''
 
# ndarray and matrix matlitpication
print(n.dot(n)*m) #same as above

# convert array to matrix using 'mat'
print(np.mat(n)**2) # convert array to matrix
'''
[[ 6  8  8]
 [16 24 20]
 [24 30 34]]
'''