# Cython/errorCalculation.pyx

import numpy as np 
import cython 

@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double errorCalculation(int bitLength, double noiseAmp):
  #uniformly distributed sequence to generate singal
  cdef:
      int  i
      double error =0.0
      double[:] b = np.random.uniform(-1, 1, bitLength)
      double[:] noise = np.random.randn(bitLength)
      double[:] signal = np.zeros(bitLength)
      double[:] rxSignal = np.zeros(bitLength)
      double[:] detectedSignal = np.zeros(bitLength)
      double[:] errorMatrix = np.zeros(bitLength)

  for i in range(bitLength):
    if b[i] < 0:
      signal[i]=-1
    else:
      signal[i]=1

  #recevied signal
  for i in range(bitLength):
      rxSignal[i] = signal[i] + noiseAmp*noise[i]
  #decision
  for i in range(bitLength):
    if rxSignal[i] < 0:
      detectedSignal[i]=-1
    else:
      detectedSignal[i]=1
  #store 1 in errorMatrix if error else 0
  for i in range(bitLength):
      errorMatrix[i] = abs((detectedSignal[i] - signal[i])/2)
  #add all 1's to find total error
  error = np.sum(errorMatrix)   
  return error