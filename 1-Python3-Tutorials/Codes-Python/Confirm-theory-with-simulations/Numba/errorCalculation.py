# Numba/serrorCalculation.py
import numpy as np 
from numba import autojit

@autojit
def errorCalculation(bitLength, noiseAmp):
  #uniformly distributed sequence to generate singal
  b = np.random.uniform(-1, 1, bitLength)

  #binary signal generated from 'b'
  signal = np.zeros((bitLength),float)
  for i in range(len(b)):
    if b[i] < 0:
      signal[i]=-1
    else:
      signal[i]=1

  #Normal Distribution: Gaussian Noise with unit variance
  noise = np.random.randn(bitLength)

  #recevied signal
  rxSignal = signal + noiseAmp*noise
  # decision:
  detectedSignal = np.zeros((bitLength),float)
  for i in range(len(b)):
    if rxSignal[i] < 0:
      detectedSignal[i]=-1
    else:
      detectedSignal[i]=1
  #error matrix: save 1 if error else 0
  errorMatrix = abs((detectedSignal - signal)/2)
  error=errorMatrix.sum() #add all 1's for total error
  return error