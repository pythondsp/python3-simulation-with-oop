# Numba/simBPSK.py
import time #to measure the simulation time
startTime=time.time()

import numpy as np
from scipy.special import erfc 
import matplotlib.pyplot as plt

#errorCalculation: calculate the error in decision
from errorCalculation import errorCalculation

def main():
  bitLength  = 100000 #number of transmitted bit
  iterLen=30 #iterate 30 time for better average

  SNRdB = np.array(np.arange(-2, 10, 1),float)
  SNR = 10**(SNRdB/10) # SNR_db to SNR conversion
  noise = np.zeros(len(SNRdB), float)

  error =np.zeros((iterLen, len(SNRdB)), float)

  for iL in range(iterLen):
    #Generate various Noise-voltage (N0/2) Level
    for i in range (len(noise)):
      noise[i]= 1/np.sqrt(2*SNR[i]) # N0/2 = 1/sqrt(2*noisePower)

    #calculate error
    errorMatrix =np.zeros(len(SNRdB), float)
    for i in range(len(noise)):
      errorMatrix[i] = errorCalculation(bitLength, noise[i])
    #save error at each iteration
    error[iL]=errorMatrix

  #average error
  BER = error.sum(axis=0)/(iterLen*bitLength)

  #theoritical error
  theoryBER = np.zeros(len(SNRdB),float)
  for i in range(len(SNRdB)):
    theoryBER[i] = 0.5*erfc(np.sqrt(SNR[i]))
  
  #print simulation time
  print(time.time()-startTime) # 8.93 sec
 
  #plot the graph
  plt.semilogy(SNRdB, BER,'--')
  plt.semilogy(SNRdB, theoryBER, 'mo')
  plt.ylabel('BER')
  plt.xlabel('SNR')
  plt.title('BPSK BER Curves')
  plt.legend(['Simulation', 'Theory'], loc='upper right')
  
  plt.grid()
  plt.show()

# Standard boilerplate to call the main() function.
if __name__ == '__main__':
  main()