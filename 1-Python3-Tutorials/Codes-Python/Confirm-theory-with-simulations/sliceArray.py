# sliceArray.py

import numpy as np

# note that in 'arange command' 
# last number i.e. 10 is not included in the list
s = np.array([np.arange(1,10)]) 
print(s) # [[1 2 3 4 5 6 7 8 9]]

# below line is known as slicing
x = s[0,3:6] #get element 3 to 5 from line 0
print(x) # [4 5 6]

s[0, 5:9] = 2 #replace number from position 5 to 9 by 2. 
print(s) # [[1 2 3 4 5 2 2 2 2]]
