# accessArray.py

import numpy as np 

# two dimensional array
b = np.array([
        [1, 2, 3],
        [4, 5, 6]
    ])

b[0,1] # 0th row and 1st column = 2

b[1, 2] = 10 # change 6 to 10
print(b)
'''
[[ 1  2  3]
 [ 4  5 10]]
'''

b[1, 1] = 15.2 #change 5 to 15.2
print(b) # note 15 is saved instead of 15.2
'''
[[ 1  2  3]
 [ 4 15 10]]
'''

c = np.array([[9, 8, 7], [3, 5, 1]], dtype=float)
c[1, 1] = 10.5 # change 5 to 10.5
print(c)
'''
[[  9.    8.    7. ]
 [  3.   10.5   1. ]]
'''