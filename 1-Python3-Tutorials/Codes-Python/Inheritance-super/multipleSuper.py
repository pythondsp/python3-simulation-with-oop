#multipleSuper.py
class A:
	def __init__(self):
		print("A")

class B:
	def __init__(self):
		print("B")		

class C(A,B):
	def __init__(self):
		A.__init__(self) # A
		B.__init__(self) # B

c=C()	