#superInheritOneClass.py
class A:
	def __init__(self):
		print("A")

class B:
	def __init__(self):
		print("B")		

# A is written before B
class C(A,B): #therefore super will inherit A
	def __init__(self):
		super().__init__() # A

#instance of C
c=C()	