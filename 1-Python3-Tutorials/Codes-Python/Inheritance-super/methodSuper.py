#methodSuper.py
class A:
	def printClass(self):
		print("A")

class B(A):
	def printClass(self):
		print("B")  

	def printName(self):
		B.printClass(self); # B
		super().printClass(); # A
  
#instant of B
b = B()
b.printName() # calling printClass