#multipleInitParameter.py
class plus5:
	def __init__(self, price="", **kwargs):
		self.value += 5
		print("price", price) # price 8
		super().__init__(**kwargs)

class multiply2:
	def __init__(self, quantity="", **kwargs):
		self.value *=  2	
		print("quantity", quantity)	# quantity 6
		super().__init__(**kwargs)

class C(multiply2, plus5):
	def __init__(self, value, **kwargs):
		self.value = value
		super().__init__(**kwargs)

#instantiate C
c=C(3, quantity=6, price=8)		
print ("Value: =", c.value) # 11 i.e. 3*2+5