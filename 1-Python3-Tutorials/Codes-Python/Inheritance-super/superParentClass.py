#superParentClass.py
class A:
	def __init__(self):
		print("reached A")
		super().__init__()
		print("A")

class B:
	def __init__(self):
		print("reached B")
		super().__init__()
		print("B")		

class D:
	def __init__(self):
		print("reached D")
		super().__init__()
		print("D")	

class C(B,A,D):
	def __init__(self):
		super().__init__() 

# instantiate the class C
c=C() # see the order of outputs
# reached B
# reached A
# reached D
# D
# A
# B