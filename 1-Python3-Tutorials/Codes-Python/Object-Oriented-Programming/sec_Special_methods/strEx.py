#strEx.py

#class declaration 
class Jungle:
    def __str__(self):
        return("It is an object of class Jungle")

def main():
    # create object of class Jungle
    j = Jungle()
    print(j)  # It is an object of class Jungle

# standard boilerplate to set 'main' as starting function
if __name__=='__main__':
    main() 
