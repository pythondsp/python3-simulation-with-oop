#dictEx.py

#class declaration 
class Jungle:
    """ List of animal and pet information 
        animal = string
        isPet = string
    """
    def __init__(self, animal="Elephant", isPet="yes"):
        self.animal =  animal
        self.isPet = isPet

def main():
    # create object of class Jungle
    j1 = Jungle()
    print(j1.__dict__) # {'isPet': 'yes', 'animal': 'Elephant'}

    j2 = Jungle("Lion", "No")
    print(j2.__dict__) # {'isPet': 'No', 'animal': 'Lion'}

    print(Jungle.__dict__)
    """ {'__doc__': '__doc__': ' List of animal and pet information \n
                        animal = string\n      isPet = string\n    ',
        '__weakref__': <attribute '__weakref__' of 'Jungle' objects>, 
        '__module__': '__main__', 
        '__dict__': <attribute '__dict__' of 'Jungle' objects>, 
        '__init__': <function Jungle.__init__ at 0x00466738>}
    """

    print(Jungle.__doc__)
    """List of animal and pet information 
        animal = string
        isPet = string
    """

# standard boilerplate to set 'main' as starting function
if __name__=='__main__':
    main() 
