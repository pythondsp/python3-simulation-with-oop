#main.py

## import class 'Jungle', 'RateJungle' and 'Test' from jungleBook.py
from jungleBook import Jungle, RateJungle, Test

def main():  
    r = RateJungle("Meher", 3)
    s = RateJungle("Krishna", 2) 

    r.averageFeedback() # Average feedback :  2.5


    # Test class is using other class attributes without inheritance
    w = Test() 
    ''' sum_of_feedback (Jungle class attribute) :  5.0
        total_num_feedback (RateJungle class attribute) :  2
    '''

# standard boilerplate to set 'main' as starting function
if __name__=='__main__':
    main()