#jungleBook.py

#class declaration 
class Jungle:
    # class attribute
    # use __sum_of_feedback to hide it from the child class
    sum_of_feedback = 0.0

    #constructor with default values
    def __init__(self, name="Unknown"):
        self._visitorName = name # please do not access directly

    def welcomeMessage(self):
        print("Hello %s, Welcome to the Jungle" % self.visitorName)

    def averageFeedback(self):
        #average feedback is hided for the the child class
        self.__avg_feedback = Jungle.sum_of_feedback/RateJungle.total_num_feedback
        print("Average feedback : ", self.__avg_feedback)

class RateJungle(Jungle):
    # class attribute
    total_num_feedback = 0

    def __init__(self, name, feedback):
        # feedback (1-10) :  1 is the best.
        self.feedback = feedback # Public Attribute

        # add new feedback value to sum_of_feedback
        Jungle.sum_of_feedback += self.feedback 
        # increase total number of feedback by 1
        RateJungle.total_num_feedback += 1 
        
        # inheriting the constructor of the class
        super().__init__(name)

    # using parent class attribute i.e. visitorName
    def printRating(self):
        print("Thanks %s for your feedback" % self._visitorName)

class Test:
    def __init__(self):
        # inheritance is not required for accessing class attribute
        print("sum_of_feedback (Jungle class attribute) : ", Jungle.sum_of_feedback)
        print("total_num_feedback (RateJungle class attribute) : ", RateJungle.total_num_feedback)


