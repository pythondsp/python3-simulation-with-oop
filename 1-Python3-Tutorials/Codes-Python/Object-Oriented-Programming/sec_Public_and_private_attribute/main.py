#main.py

# import class 'Jungle' and 'RateJungle' from jungleBook.py
from jungleBook import Jungle, RateJungle

def main():  
    ## create object of class Jungle
    j = Jungle("Meher")
    j.welcomeMessage() # Hello Meher, Welcome to the Jungle

    r = RateJungle(3)
    r.printRating() # Feedback : 3  Guide Rating: 110  Staff Rating: 55 

    # _staffRating can be accessed "directly", but not a good practice. 
    # Use the method which is provided by the author
    # e.g. below is the bad practice
    r._staffRating = 30 # directly change the _staffRating
    print("Staff rating : ", r._staffRating) # Staff rating :  30

    ## access to private attribute is not allowed
    ## uncomment following line to see error
    # print("Jungle Guide rating : ", r.__jungleGuideRating)

    ## private attribute can still be accessed as below, 
    ## objectName._className.__attributeName
    print ("Guide rating : ",  r._RateJungle__jungleGuideRating) # Guide rating :  110

# standard boilerplate to set 'main' as starting function
if __name__=='__main__':
    main()