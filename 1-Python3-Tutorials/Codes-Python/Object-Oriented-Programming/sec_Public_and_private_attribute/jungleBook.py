#jungleBook.py

#class declaration 
class Jungle:
    #constructor with default values
    def __init__(self, name="Unknown"):
        self.visitorName = name

    def welcomeMessage(self):
        print("Hello %s, Welcome to the Jungle" % self.visitorName)

class RateJungle:
    def __init__(self, feedback):
        # feedback (1-10) :  1 is the best.
        self.feedback = feedback # Public Attribute

        # Public attribute with single underscore sign 
        # Single _ signifies that author does not want to acecess it directly
        self._staffRating = 50 

        self.__jungleGuideRating = 100 # Private Attribute

        self.updateStaffRating() # update Staff rating based on feedback
        self.updateGuideRating() # update Guide rating based on feedback

    def printRating(self):
        print("Feedback : %s \tGuide Rating: %s \tStaff Rating: %s "
            % (self.feedback, self.__jungleGuideRating, self._staffRating))

    def updateStaffRating(self):
        """ update Staff rating based on visitor feedback"""
        if self.feedback < 5 :
            self._staffRating += 5
        else:
            self._staffRating -= 5

    def updateGuideRating(self):
        """ update Guide rating based on visitor feedback"""
        if self.feedback < 5 :
            self.__jungleGuideRating += 10
        else:
            self.__jungleGuideRating -= 10