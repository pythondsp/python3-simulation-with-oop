#scarySound.py

class Animal:
    def scarySound(self):
        print("Animals are running away due to scary sound.")

class Bird:
    def scarySound(self):
        print("Birds are flying away due to scary sound.")

# scaryScound is not defined for Insect
class Insect:
    pass