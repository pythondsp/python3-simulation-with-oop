#jungleBook.py

from abc import ABCMeta, abstractmethod

#Abstract class and abstract method declaration 
class Jungle(metaclass=ABCMeta):
    #constructor with default values
    def __init__(self, name="Unknown"):
        self.visitorName = name

    def welcomeMessage(self):
        print("Hello %s, Welcome to the Jungle" % self.visitorName)

    # abstract method is compulsory to defined in child-class
    @abstractmethod 
    def scarySound(self):
        pass