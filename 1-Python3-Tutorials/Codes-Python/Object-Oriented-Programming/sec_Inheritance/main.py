#main.py

## import class 'Jungle' and 'RateJungle' from jungleBook.py
from jungleBook import Jungle, RateJungle

def main():  
    r = RateJungle("Meher", 3)

    r.printRating() # Thanks Meher for your feedback

    # calling parent class method
    r.welcomeMessage() # Hello Meher, Welcome to the Jungle

# standard boilerplate to set 'main' as starting function
if __name__=='__main__':
    main()