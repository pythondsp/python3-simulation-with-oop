\section{Data generation with Numpy}
In this tutorial, Numpy library is used to generate the data for plotting. For this purpose, only 5 functions of numpy library are used, which are shown in Listing \ref{py_matplotlib:numpyDataEx}, 

\lstinputlisting[
caption    = {Data generation using Numpy},
label      = {py_matplotlib:numpyDataEx}
]{numpyDataEx.py}


\section{Basic Plots}

In this section, basic elements of the plot e.g. Title, axis names and grid etc. are discussed. 

\subsection{First Plot}
Listing \ref{py_matplotlib:firstPlot} plots the sin(x) as shown in Fig. \ref{fig:firstPlot}.
\lstinputlisting[
caption    = {Sin(x), Fig. \ref{fig:firstPlot}},
label      = {py_matplotlib:firstPlot}
]{firstPlot.py}

\begin{explanation}[Listing \ref{py_matplotlib:firstPlot}]
	Here, line 8 generates 100 equidistant points in the range [$-2\pi$, $2\pi$]. Then line 9 calculates the sine values for those points. Line 10 plots the figure, which is displayed on the screen using line 11. 
\end{explanation}

\begin{figure}[!h]
	\begin{subfigure}{0.5\textwidth}
	\includegraphics[scale=0.4]{firstPlot}
	\caption{Sin(x), Listing \ref{py_matplotlib:firstPlot}}
	\label{fig:firstPlot}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[scale=0.4]{firstPlotGL}
		\caption{Grid, Axes, Label and Legend, Listing \ref{py_matplotlib:firstPlotGL}}
		\label{fig:firstPlotGAL}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[scale=0.4]{firstPlotLineMarker}
		\caption{Line-style and Line-marker, Listing \ref{py_matplotlib:firstPlotLineMarker}}
		\label{fig:firstPlotLineMarker}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[scale=0.4]{completeBasicEx}
		\caption{Axis-limit and Axis-marker, Listing \ref{py_matplotlib:completeBasicEx}}
		\label{fig:completeBasicEx}
	\end{subfigure}
	\caption{Sin(x) Plots}
\end{figure}


\subsection{Label, Legend and Grid}

Here Listing \ref{py_matplotlib:firstPlot} is modified as shown in Listing \ref{py_matplotlib:firstPlotGL}, to add labels, legend and grid to the plot.
\lstinputlisting[
caption    = {Grid, Label and Legend, Fig. \ref{fig:firstPlotGAL}},
label      = {py_matplotlib:firstPlotGL}
]{firstPlotGL.py}
\begin{explanation}[Listing \ref{py_matplotlib:firstPlotGL}]
In line 11, ``label=`sin' '' is added which is displayed by `legend' command in line 15. ``loc=best'' is optional parameter in line 15. This parameter find the best place for the legend i.e. the place where it does not touch the plotted curve. Line 18 and 19 add x and y label to curves. Finally, line 21 adds the grid-lines to the plot. 

For changing the location of legend, replace `best' in line 15 with `center', `center left', `center right', `lower center', `lower left', `lower right', `right', `upper center', `upper left' or `upper right'.
\end{explanation}

\subsection{Line style and Marker}
It is good to change the line styles and add markers to the plots with multiple graphs. It can be done as shown in Listing \ref{py_matplotlib:firstPlotLineMarker}.

\lstinputlisting[
caption    = {Line Style and Marker, Fig. \ref{fig:firstPlotLineMarker}},
label      = {py_matplotlib:firstPlotLineMarker}
]{firstPlotLineMarker.py}

\begin{explanation}[Listing \ref{py_matplotlib:firstPlotLineMarker}]
In line 13, `*- -r' is the combination of three separate parameters i.e. `*', `- -' and `r', which represents `marker', `line style' and `color' respectively. We can change the order of these combinations e.g. `r- -*' and `- -r*' etc. Also, combination of two parameters (e.g. `r- -') or single parameter (e.g. `- -') are valid. 

\begin{table}
	\centering
	\includegraphics{TableLineStyle}
	\caption{Line Style}
	\label{tbl:TableLineStyle}
\end{table}

\begin{table}
	\centering
	\includegraphics{TableMarkerStyle}
	\caption{Marker Style}
	\label{tbl:TableMarkerStyle}
\end{table}

\begin{table}
	\centering
	\includegraphics{TableColorStyle}
	\caption{Color Style}
	\label{tbl:TableColorStyle}
\end{table}

\begin{table}
	\centering
	\includegraphics{TablePlotStyle}
	\caption{Plot Style}
	\label{tbl:TablePlotStyle}
\end{table}

Table \ref{tbl:TableLineStyle}, \ref{tbl:TableMarkerStyle} and \ref{tbl:TableColorStyle} show some more abbreviations for `line style', `marker style' and `color' respectively. Note that, only one element can be chosen from each style to make the combination. 

Further, line 13 contains `markersize' parameter for changing the size of the marker. Table \ref{tbl:TablePlotStyle} shows the complete list of additional parameters to change the plot style. Lastly, line 13 can be rewritten using complete features of the plot as follows, 

\textbf{plt.plot(x, sinx, color='m', \\
	linestyle='-.', linewidth=4, \\
	 marker='o', markerfacecolor='k', markeredgecolor='g', \\
	 markeredgewidth=3, markersize=5, \\
	label='sin'
)}
\end{explanation}

\subsection{Axis and Title}

Listing \ref{py_matplotlib:completeBasicEx} adds axis and title in previous figures. 

\lstinputlisting[
caption    = {Title and Axis, Fig. \ref{fig:completeBasicEx}},
label      = {py_matplotlib:completeBasicEx}
]{completeBasicEx.py}

\begin{explanation}[Listing \ref{py_matplotlib:completeBasicEx}]
Lines 25 and 26 in the listing add display range for x and y axis respectively in the plot as shown in Fig. \ref{fig:completeBasicEx}. Line 30 and 32 add the ticks on these axis. Further, line 31 adds various `display-name' for the ticks. It changes the display of ticks e.g. `np.pi' is normally displayed as `3.14', but  ``$r' + \backslash pi$'' will display it as $+\pi$. Note that `$\backslash pi$' is the ``latex notation'' for $\pi$, and matplotlib supports latex notation as shown in various other examples as well in the tutorial. 	
\end{explanation}

\section{Multiple Plots}
In this section various mutliplots are discussed e.g. plots on the same figure window and subplots etc. 

\subsection{Mutliplots in same window}
By default, matplotlib plots all the graphs in the same window by overlapping the figures. In Listing \ref{py_matplotlib:multiplot}, line 14 and 15 generate two plots, which are displayed on the same figure window as shown in Fig. \ref{fig:multiplot}.
  
\lstinputlisting[
caption    = {Mutliplots in same window, Fig. \ref{fig:multiplot}},
label      = {py_matplotlib:multiplot}
]{multiplot.py}

\begin{figure}[!h]
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[scale=0.4]{multiplot}
		\caption{Multiplots, Listing \ref{py_matplotlib:multiplot}}
		\label{fig:multiplot}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[scale=0.4]{subplotEx}
		\caption{Subplots, Listing \ref{py_matplotlib:subplotEx}}
		\label{fig:subplotEx}
	\end{subfigure}
	\caption{Multiple plots}
\end{figure}

\subsection{Subplots}
In Fig. \ref{fig:multiplot}, two plots are displayed in the same window. Further, we can divide the plot window in multiple sections for displaying each figure in different section as shown in Fig. \ref{fig:subplotEx}. 
\\
\\
\lstinputlisting[
caption    = {Subplots, Fig. \ref{fig:subplotEx}},
label      = {py_matplotlib:subplotEx}
]{subplotEx.py}

\begin{explanation}[Listing \ref{py_matplotlib:subplotEx}]
Subplot command takes three parameters i.e. number of rows, numbers of columns and location of the plot. For example in line 14, subplot(2,1,1),  divides the figure in 2 rows and 1 column, and uses location 1 (i.e. top) to display the figure. Similarly, in line 21, subplot(2,1,2) uses the location 2 (i.e. bottom) to plot the graph. Further, Listing \ref{py_matplotlib:histogramEx} divides the figure window in 4 parts and then location 1 (top-left), 2 (top-right), 3 (bottom-left) and 4 (bottom-right) are used to display the graphs. 

Also, all the plot commands between line 14 and 21, e.g. line 15, will be displayed on the top location. Further, plots defined below line 21 will be displayed by bottom plot window.
\end{explanation}


\begin{figure}[!h]
	\begin{subfigure}{0.5\textwidth}
	\centering
	\includegraphics[scale=0.4]{multiplotDifferentWindow}
	\caption{Figure window with name ``Sin''}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[scale=0.42]{multiplotDifferentWindow2}
		\caption{Figure window with name ``Cos''}
	\end{subfigure}
	\caption{Open figures in separate windows, Listing \ref{py_matplotlib:multiplotDifferentWindow}}
	\label{fig:multiplotDifferentWindow}
\end{figure}

\subsection{Mutliplots in different windows}
`figure()' command is used to plot the graphs in different windows,  as shown in line 17 of Listing \ref{py_matplotlib:multiplotDifferentWindow}.

\lstinputlisting[
caption    = {Mutliplots in different windows, Fig. \ref{fig:multiplotDifferentWindow}},
label      = {py_matplotlib:multiplotDifferentWindow}
]{multiplotDifferentWindow.py}

\begin{explanation}[Listing \ref{py_matplotlib:multiplotDifferentWindow}]
Here optional name ``Sin'' is given to the plot which is displayed on the top in Fig. \ref{fig:multiplotDifferentWindow}. Then line 19 opens a new plot window with name ``Cos''. Finally in line 21, the name ``Sin'' is used again, which selects the previously open ``Sin'' plot window and plot the figure there. Hence, we get two plots in this window (i.e. from lines 18 and 22) as show in Fig. \ref{fig:multiplotDifferentWindow}.
\end{explanation}

\section{Semilog Plot}
Semilog plots are the plots which have y-axis as log-scale and x-axis as linear scale as shown in Fig. \ref{fig:semilog}. Listing \ref{py_matplotlib:semilogEx} plots both the semilog and linear plot of the function $e^x$. 

\lstinputlisting[
caption    = {Semilog plot Fig. \ref{fig:semilogEx}},
label      = {py_matplotlib:semilogEx}
]{semilogEx.py}

\begin{figure}[!h]
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[scale=0.4]{semilogEx2}
		\caption{Linear Plot}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[scale=0.42]{semilogEx}
		\caption{Semilog Plot}
		\label{fig:semilog}
	\end{subfigure}
	\caption{Semilog Plot vs Linear Plot \ref{py_matplotlib:semilogEx}}
	\label{fig:semilogEx}
\end{figure}