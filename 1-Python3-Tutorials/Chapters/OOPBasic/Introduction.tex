\section{Introduction}
Object oriented programming (OOP) increases the re-usability of the code. Also, the codes become more manageable than non-OOP methods. But, it takes proper planning, and therefore longer time, to write the codes using OOP method. In this chapter, we will learn various terms used in OOP along with their usages with examples. 

\section{Class and object}
A `class' is user defined template which contains variables, constants and functions etc.; whereas an `object' is the instance (or variable) of the class. In simple words, a class contains the structure of the code, whereas the object of the class uses that structure for performing various tasks, as shown in this section.

\subsection{Create class and object}
Class is created using keyword `class' as shown in Line 4 of Listing \ref{py_oopsBasics:ex1}, where the class `Jungle' is created. \textbf{As a rule, class name is started with uppercase letter, whereas function name is started with lowercase letter}. Currently, this class does not have any structure, therefore keyword `pass' is used at Line 5. Then, at Line 8, an object of class i.e. `j' is created; whose value is printed at Line 9. This print statement prints the class-name of this object along with it's location in the memory (see comments at Line 9).    
\lstinputlisting[
caption    = {Create class and object},
language = Python,
label      = {py_oopsBasics:ex1}
]{sec_Create_class_and_object/ex1.py}

\subsection{Add function to class}
Now, we will add one function `welcomeMessage' in the class. The functions inside the class are known as `\textbf{methods}'. The functions inside the class are the normal functions (nothing special about them), as we can see at Lines 5-6. \textbf{To use the variables and functions etc. outside the class, we need to create the object of the class first, as shown in Line 9, where object `j' is created}. When we create teh object of a class, then all the functions and variables of that class is attached to the object and can be used by that object; e.g. the object `j' can now use the function `welcomeMessage' using `.' operator, as shown in Line 10. Also, `self' is used at Line 6, which is discussed in Section \ref{sec:Constructor}. 
\lstinputlisting[
caption    = {Add function to class},
language = Python,
label      = {py_oopsBasics:ex2}
]{sec_Create_class_and_object/ex2.py}

\subsection{Constructor} \label{sec:Constructor}
The `\_\_init\_\_' method is used to define and initialize the class variables. This ``\_\_init\_\_' method is known as \textbf{Constructor} and the variables are known as \textbf{attributes}. Note that, the \textbf{self} keyword is used in the `init function' (Line 6) along with the name of the variables (Line 7). Further All the functions, should have first parameter as `self' inside the class. Although we can replace the word `self' with any other word, but it is good practice to use the word `self' as convention. 

\begin{explanation}[Listing \ref{py_oopsBasics:ex3}]
	Whenever, the object of a class is create then all the attributes and methods of that class are attached to it; and \textbf{the constructor i.e. `\_\_init\_\_' method is executed automatically}. Here, the constructor contains one variable i.e. `visitorName' (Line 7) and one input parameter i.e. `name' (Line 6) whose value is initialized with `unknown'. Therefore, when the object `j' is created at Line 13, the value `Meher' will be assigned to parameter `name' and finally saved in `visitorName' as constructor is executed as soon as the object created. Further, if we create the object without providing the name i.e. `j = Jungle()', then default value i.e. `unknown' will be saved in attribute `visitorName'. Lastly, the method `welcomeMessage' is slightly updated, which is now printing the name of the `visitor' (Line 10) as well. 
\end{explanation}
\lstinputlisting[
caption    = {Constructor with default values},
language = Python,
label      = {py_oopsBasics:ex3}
]{sec_Create_class_and_object/ex3.py}

\subsection{Define `main' function}
The above code can be written in `main' function (Lines 12-19) using standard-boiler-plate (Lines 22-23), which makes the code more readable, as shown in Listing \ref{py_oopsBasics:ex3}. This boiler-plate is discussed in Section \ref{main_th_ber}, which tells the python-interpretor that the `main' is the starting point of the code. 
\lstinputlisting[
caption    = {Constructor with default values},
language = Python,
label      = {py_oopsBasics:ex3}
]{sec_Create_class_and_object/ex4.py}

\subsection{Keep classes in separate file}
To make code more manageable, we can save the class-code (i.e. class Jungle) and application-files (i.e. main) in separate file. For this, save the class code in `jungleBook.py' file, as shown in Listing \ref{py_oopsBasics:sc_jungleBook}; whereas save the `main()' in `main.py' file as shown in Listing \ref{py_oopsBasics:sc_main}. Since, class is in different file now, therefore we need to import the class to `main.py file' using keyword `\textbf{import}' as shown in Line 4 of Listing \ref{py_oopsBasics:sc_main}. 
\lstinputlisting[
caption    = {Save classes in separate file},
language = Python,
label      = {py_oopsBasics:sc_jungleBook}
]{sec_Create_class_and_object/jungleBook.py}

\lstinputlisting[
caption    = {Import class to main.py},
language = Python,
label      = {py_oopsBasics:sc_main}
]{sec_Create_class_and_object/main.py}

\section{Inheritance}
Suppose, we want to write a class `RateJungle' in which visitor can provide `rating' based on their visiting-experience. If we write the class from the starting, then we need define attribute `visitorName' again; which will make the code repetitive and unorganizable, as the visitor entry will be at multiple places and such code is more prone to error. With the help of inheritance, we can avoid such duplication as shown in Listing \ref{py_ih_jungleBook}; where class Jungle is inherited at Line 12 by the class `RateJungle'. Now, when the object `r' of class `RateJungle' is created at Line 7 of Listing \ref{py_ih_main}, then this object `r' will have the access to `visitorName' as well (which is in the parent class). Note that, inheritance is discussed in detail in Chapter \ref{InheritanceSuper}.

\lstinputlisting[
caption    = {Inheritance},
language = Python,
label      = {py_ih_jungleBook}
]{sec_Inheritance/jungleBook.py}

\lstinputlisting[
caption    = {Usage of parent-class method and attributes in child-class},
language = Python,
label      = {py_ih_main}
]{sec_Inheritance/main.py}

\section{Polymorphism}
In OOP, we can use same name for methods and attributes in different classes; the methods or attributes are invoked based on the object type; e.g. in Listing \ref{py_oopsBasics:p_scarySound}, the method `scarySound' is used for class `Animal' and `Bird' at Lines 4 and 8 respectively. Then object of these classes are created at Line 8-9 of Listing \ref{py_oopsBasics:p_main}.  Finally, method `scarySound' is invoked at Lines 12-13; here Line 13 is the object of class Animal, therefore method of that class is invoked and corresponding message is printed. Similarly, Line 14 invokes the `scaryMethod' of class Bird and corresponding line is printed. 
\lstinputlisting[
caption    = {Polymorphism example with function `move'},
language = Python,
label      = {py_oopsBasics:p_scarySound}
]{sec_Polymorphism/scarySound.py}

\lstinputlisting[
caption    = {Polymorphism: move function works in different ways for different class-objects},
language = Python,
label      = {py_oopsBasics:p_main}
]{sec_Polymorphism/main.py}

\section{Abstract class and method}
Abstract classes are the classes which contains one or more abstract method; and abstract methods are the methods which does not contain any implemetation, but the child-class need to implement these methods otherwise error will be reported. In this way, we can force the child-class to implement certain methods in it. We can define, abstract classes and abstract method using keyword `ABCMeta' and `abstractmethod' respectively, as shown in Lines 6 and 15 respectively of Listing \ref{py_ac_jungleBook}. Since, `scarySound' is defined as abstractmethod at Line 15-17, therefore it is compulsory to implement it in all the subclasses. \textbf{Look at the class `Insect' in Listing \ref{py_oopsBasics:p_scarySound}, where `scarySound' was not defined but code was running correctly; but now the `scarySound' is abstractmethod, therefore it is compulsory to implement it, as done in Line 16 of Listing \ref{py_ac_scarySound}}.
\lstinputlisting[
caption    = {Abstract class and method},
language = Python,
label      = {py_ac_jungleBook}
]{sec_Abstract_class_and_method/jungleBook.py}

\lstinputlisting[
caption    = {Abstract methods are compulsory to define in child-class},
language = Python,
label      = {py_ac_scarySound}
]{sec_Abstract_class_and_method/scarySound.py}

\lstinputlisting[
caption    = {Main function},
language = Python,
label      = {py_oopsBasics:ac_main}
]{sec_Abstract_class_and_method/main.py}

\section{Public and private attribute}
\textbf{There is not concept of private attribute in Python.} All the attributes and methods are accessible to end users. But there is a convention used in Python programming i.e. if a variable or method name starts with `\_', then users should not directly access to it; there must be some methods provided by the class-author to access that variable or method.  Similarly, `\_\_' is designed for renaming the attribute with class name i.e. the attribute is automatically renamed as `\_className\_\_attributeName'. This is used to avoid conflict in the attribute names in different classes, and is useful at the time of inheritance, when parent and child class has same attribute name. 

 
Listing \ref{py_oopsBasics:pp_jungleBook} and \ref{py_oopsBasics:pp_main} show the example of attributes along with the methods to access them. Please read the comments to understand these codes. 

\lstinputlisting[
caption    = {Public and private attribute},
language = Python,
label      = {py_oopsBasics:pp_jungleBook}
]{sec_Public_and_private_attribute/jungleBook.py}

\lstinputlisting[
caption    = {Accessing public and private attributes},
language = Python,
label      = {py_oopsBasics:pp_main}
]{sec_Public_and_private_attribute/main.py}

\section{Class Attribute}
Class attribute is the variable of the class (not of method) as shown in Line 7 of Listing \ref{py_oopsBasics:ih_main}. This attribute can be available to all the classes without any inheritance e.g. At Line 44, the class Test (Line 41) is using the class-attribute `sum\_of\_feedback' of class Jungle (Line 7). Note that, we need to use the class name to access the class attribute e.g. Jungle.sum\_of\_feedback (Lines 30 and 44).
\lstinputlisting[
caption    = {Class attributes and it's access},
language = Python,
label      = {py_oopsBasics:ih_main}
]{sec_Class_Attribute/jungleBook.py}

\lstinputlisting[
caption    = {Main program},
language = Python,
label      = {py_oopsBasics:ih_main}
]{sec_Class_Attribute/main.py}


\section{Special methods}
There are some special method, which are invoked under certain cases e.g. \_\_init\_\_ method is invoked, when an object of the instance is created. In this section, we will see some more special methods.

\subsection{\_\_init\_\_ and \_\_del\_\_}
The \_\_init\_\_ method is invoked when object is created; whereas \_\_del\_\_ is always invoked at the end of the code; e.g. we invoke the `del' at Line 21 of Listing \ref{py_oopsBasics:sm_delEx}, which deletes object `s1' and remaining objects are printed by Line 13. But, after Line 25, there is no further statement, therefore the `del' command will automatically executed, and results at Lines 31-32 will be displayed. The `del' command is also known as `\textbf{destructor}'. 
\lstinputlisting[
caption    = {\_\_init\_\_ and \_\_del\_\_ function},
language = Python,
label      = {py_oopsBasics:sm_delEx}
]{sec_Special_methods/delEx.py}


\subsection{\_\_str\_\_}
When \_\_str\_\_ is defined in the class, then `print' statement for object (e.g. print(j) at Line 11 of Listing \ref{py_oopsBasics:sm_strEx}), will execute the \_\_str\_\_ statement, instead of printing the address of object, as happened in Listing \ref{py_oopsBasics:ex1}. This statement is very useful for providing the useful information about the class using print statement. 
\lstinputlisting[
caption    = {\_\_str\_\_ method is executed when `print' statement is used for object},
language = Python,
label      = {py_oopsBasics:sm_strEx}
]{sec_Special_methods/strEx.py}


\subsection{\_\_call\_\_}
The \_\_call\_\_ method is executed, when object is used as function, as shown in Line 20 of Listing \ref{py_oopsBasics:sm_callEx}; where object `d' is used as function i.e. d(300). 
\lstinputlisting[
caption    = {\_\_call\_\_ method is executed when object is used as function},
language = Python,
label      = {py_oopsBasics:sm_callEx}
]{sec_Special_methods/callEx.py}

\subsection{\_\_dict\_\_ and \_\_doc\_\_}
\_\_dict\_\_ is used to get the useful information about the class (Line 21); whereas \_\_doc\_\_ prints the docstring of the class (Line 30). 
\lstinputlisting[
caption    = {\_\_dict\_\_ and \_\_doc\_\_},
language = Python,
label      = {py_oopsBasics:sm_dictEx}
]{sec_Special_methods/dictEx.py}


\subsection{\_\_setattr\_\_ and \_\_getattr\_\_}
Method \_\_setattr\_\_ is executed, whenever we set the value of an attribute. \_\_setattr\_\_ can be useful for validating the input-types before assigning them to attributes as shown in Line 9 of Listing \ref{py_oopsBasics:sm_setAttrEx}. Please read the comments of Listing \ref{py_oopsBasics:sm_setAttrEx} for better understanding. Similarly, \_\_getattr\_\_ is invoked whenever we try to access the value of an attribute, \textbf{which is not in the dictionary}. 

\lstinputlisting[
caption    = {\_\_setattr\_\_ and \_\_getattr\_\_},
language = Python,
label      = {py_oopsBasics:sm_setAttrEx}
]{sec_Special_methods/setAttrEx.py}

%\subsection{\_\_getattribute\_\_ vs \_\_getattr\_\_}

\section{Conclusion}
In this chapter, we learn various features of object oriented programming in Python. We saw that there is no concept of private attributes in Python. Lastly, we discuss various special methods available in Python which can enhance the debugging and error checking capability of the code. 