\section{System model}\label{sec:SystemModel}
Fig. \ref{fig:BlockDiagram} shows the block diagram of the proposed system. In this system, there are four blocks i.e. transmitter, noisy environment, receiver and decision blocks. 

\begin{figure}
	\centering
	\includegraphics[scale=0.9]{BlockDiagram}
	\caption{Block diagram of the proposed system}
	\label{fig:BlockDiagram}
\end{figure}


\begin{explanation}[Fig. \ref{fig:BlockDiagram}]
	In this system, the transmitter (on the left side) transmits the series of random data with values $\pm 1$. Then noise is added by the environment,  which is modeled as Gaussian distributed data. Hence, received signal is not same as the transmitted signal. Next, we need to make the decision about the transmitted signals (based on received signals, whose values are no longer $\pm 1$, due to addition of noise) as shown in the decision block; i.e. if the received signal is greater than zero then transmitted signal is 1 otherwise it is zero.  Based on this process, the final theoretical bit error rate (BER) equation is given by, 
	\begin{equation}
	{BER} = \frac{1}{2}erfc\left( {\sqrt {SNR} } \right)
	\label{eq:theoryBPSK}
	\end{equation}  
	where, `$SNR$' and `$erfc$' are the signal to noise ratio and complementary error function respectively. 
\end{explanation}
\begin{noNumBox}
Theories are verified using simulations with following steps, 
\begin{enumerate}
	\item First, we need to plot the theoretical results, i.e. BER equation which is given in equation (\ref{eq:theoryBPSK}) in this example. 
	\item Next, we need to simulate the block diagram and plot the results, with procedure given in explanation.
	\item If above two graphs match each other, then we can say that the theoretical result is correct for the proposed system in Fig. \ref{fig:BlockDiagram}. 
\end{enumerate}
\end{noNumBox}

\section{Theoretical BER} \label{main_th_ber}

In this section, theoretical BER of BPSK system is plotted which is given by equation \ref{eq:theoryBPSK}. Listing \ref{py_thSim:theoryBPSK} is the python code for plotting the BER equation.  

\begin{explanation}[Listing \ref{py_thSim:theoryBPSK}]
	Equation \ref{eq:theoryBPSK} is written at line 16 of the listing. BER values are calculated for 10 different SNR values i.e. '-2dB to 9dB' as shown in line 10 of the code. In line 11, dB values are converted into normal power scale. Then, these values are used at line 16 to calculate the BER values. Note that, lines 28-29 tell python-interpretor that main() function should be executed first. 
\end{explanation}

\lstinputlisting[
caption    = {Theoretical BER of BPSK system in Gaussian Noise, Fig. \ref{fig:theoryBPSK}},
language = Python,
label      = {py_thSim:theoryBPSK}
]{theoryBPSK.py}

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{theoryBPSK}
	\caption{Theoretical BER of BPSK system in Gaussian Noise, Listing \ref{py_thSim:theoryBPSK}}
	\label{fig:theoryBPSK}
\end{figure}

\section{Confirm theory with simulation} \label{sec:confirm_th_with_sim}
In previous section, only theoretical result was plotted using python. In this section, block diagram of the system i.e. Fig. \ref{fig:BlockDiagram} is also simulated as shown in Listing \ref{py_thSim:simBPSK} and \ref{py_thSim:errorCalculation}. Further, these listing are converted into cython, numba and matlab codes as well, for increasing the speed of the simulations. These codes are given in the Appendix \ref{app:CythonNumbaBPSK}. Simulation times for python, cython, numba and matlab 	are compared in Table \ref{tbl:SimTime}.

\begin{table}[!h]
	\centering
	\includegraphics[scale=0.8]{SimTime}
	\caption{Simulation time comparison: Python, Numba and Cython}
	\label{tbl:SimTime}
\end{table} 

\begin{explanation}[Listing \ref{py_thSim:simBPSK} and \ref{py_thSim:errorCalculation}]
	In the Listing \ref{py_thSim:simBPSK}, time function in line 3-4 are used to measure the time of simulation. Further in line 11, first `errorCalculation' is the name of the python file, in which second `errorCalculation' is defined as function, as shown in Listing  \ref{py_thSim:errorCalculation}. 
	
	The simulator is iterated 30 times as shown in line 15 of Listing \ref{py_thSim:simBPSK}, then average of these 30 values are taken in line 35. The function `errorCalculation' (line 11) calculates the BER values for each iteration, which are stored in `error' matrix (line 32). 
	
	SNR is defined as ratio of signal and noise power i.e. $SNR = \frac{E_b }{N_0}$, where $E_b$ is the signal power and $\frac{N_0}{2}$ is the noise power. Since, the power of the signal is considered as 1, therefore $SNR = \frac{1}{{N_0}}$. Line 25 converts the noise powers into noise voltages i.e. $\sqrt{\frac{{{N_0}}}{2}} = \sqrt {\frac{1}{{2*SNR}}} $, as we need to add the noise to the signal as shown in Fig. \ref{fig:BlockDiagram}. These values of noise are send to Listing \ref{py_thSim:errorCalculation} through line 31. 
	
	Next, Listing \ref{py_thSim:errorCalculation} uses above calculated noise values as noiseAmp (line 5). In line 18, Gaussian noise is generated using `randn' function (i.e. normal random distribution function). Note that,  Normal distribution is nothing but the Gaussian distribution with zero mean and unit variance. Also, transmitted bits (i.e. 0 and 1) are generated randomly in line 7, which are converted into $\pm 1$ in lines 11-15. Next, different noise levels are added to signal in line 21. Line 24-28 make decision about the transmitted bit according to decision rule shown in Fig. \ref{fig:BlockDiagram}. Line 30 compares the decision with actual transmitted bit and set the value to 1 if decision is wrong, otherwise set it to zero. Finally, all these 1's are added (line 31) to calculate the total number of wrong decisions and the value is return to Listing \ref{py_thSim:simBPSK}. 
	
	This process is repeated 30 time (line 15) in Listing \ref{py_thSim:simBPSK} and average to these errors are taken in line 36 \footnote{The process of repeating the simulation various times and then taking the average at the end is known as Monte-Carlo simulation.}, which is the plotted by lines 47-55. Finally, we find that plot of theoretical BER (line 39-41) matches with the simulation as shown in Fig. \ref{fig:simBPSK}. Therefore we can say that the theoretical result is correct,  as it is verified by the simulator (which is created according to proposed system in Fig. \ref{fig:BlockDiagram})
	
	
\end{explanation}

\lstinputlisting[
caption    = {Simulation BER of BPSK system in Gaussian Noise, Fig. \ref{fig:simBPSK}},
language = Python,
label      = {py_thSim:simBPSK}
]{simBPSK.py}

\begin{figure}
	\centering
	\includegraphics[scale=0.45]{simBPSK}
	\caption{Theory and Simulation BER are overlapped, Listing \ref{py_thSim:simBPSK}}
	\label{fig:simBPSK}
\end{figure}

\lstinputlisting[
caption    = {Calculate error for each iteration},
language = Python,
label      = {py_thSim:errorCalculation}
]{errorCalculation.py}

