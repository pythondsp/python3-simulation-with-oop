\section{Introduction}
In this chapter, `super' method is discussed for inheritance in Python. Further, multiple inheritance with different parameters in the `init' function, is also discussed. 

\section{Inherit `\_\_init\_\_' from parent class}
If we want to inherit the `\_\_init\_\_()' function of the parent class, then `super' function is required. In Listing \ref{py_inSup:initSuper}, the class `rectArea' is inheriting the \_\_init\_\_ function of the class `reacLen'. After inheriting the value of length, the area is calculated. 

\begin{explanation}[Listing \ref{py_inSup:initSuper}]
	In the listing, two classes are defined at Lines 2 and 6. The class `rectLen' (Line 2) contains the `init' function which initialize the length variable. The other class i.e. `rectArea' inherits the `rectLen' class at line 6. Then in Line 7, the `rectArea' class inherits the `init' function using `super' command, which initializes the value of `length'. Next  Line 8, initializes the value of `width' and then area is calculated at Line 9. Note that, `return' function can not be used inside the `init' method i.e. `init' can not return any value. If we uncomment the line 11, Python interpretor will generate the error. 
\end{explanation}
\lstinputlisting[
caption    = {Inherit `\_\_init\_\_' function from parent class},
language = Python,
label      = {py_inSup:initSuper}
]{initSuper.py}

\section{Inherit `methods' from parent class}
If we have same method names in `parent' and `child' classes, then Python by default uses the `child class' method. In such cases, `super' function is required to use the `parent class' method in `child class', as shown in Listing \ref{py_inSup:methodSuperSuper}. 

\begin{explanation}[Listing \ref{py_inSup:methodSuperSuper}]
	In the listing, the parent class `A' and the child class `B' have the same method name i.e. `printClass' at Lines 3 and 7. Line 15 creates a instance of class B, and then `printName' method is called at Line 16. The `printName' class at Line 10, first calls the `printClass' method of `class B' at Line 11; and then in Line 12, it uses the `super' function to call the `printClass' method of parent class. 
\end{explanation}
\lstinputlisting[
caption    = {Inherit `method' from parent class for same method names},
language = Python,
label      = {py_inSup:methodSuperSuper}
]{methodSuper.py}

\section{Inherit multiple classes} \label{inheritMultipleClass}
In this section, multiple classes are inherited by the child class. Careful coding is required for such cases, because for smaller codes, we may get correct results using wrong and easy methods. But, in complex codes, such wrong practices may generate errors. In following sections, some of the problems are discuss, along with the final correct solution of the problems.

\subsection{Problem 1: super() calls only one class \_\_init\_\_}
Note that, super() function can call the `init' function of only one class at a time. If Listing \ref{py_inSup:superInheritOneClass}, class C (Line 11) is inheriting class A and B. But the super() command only inherit the \_\_init\_\_ function of class A because it appeared first in Line 11. Therefore, output of Line 13, will be `A'. Similarly, if we replace the `C(A,B)' with `C(B,A)' at line 11, then output of Line 13 will be `B'. 

\lstinputlisting[
caption    = {`super' inherit only one `init' function},
language = Python,
label      = {py_inSup:superInheritOneClass}
]{superInheritOneClass.py}

\subsection{Wrong solution to problem 1}
To Inherit both the \_\_init\_\_ function, we can call each \_\_init\_\_ function explicitly as shown in Listing \ref{py_inSup:multipleSuper}. In this Listing, two `super' functions are used to call the `init' function of two classes. This solution looks correct, but may create problems as shown in Section \ref{MathProblem}.  

\lstinputlisting[
caption    = {`super' inherit only one `init' function},
language = Python,
label      = {py_inSup:multipleSuper}
]{multipleSuper.py}

\subsection{Correct solution}\label{superCorrectSolution}
We need to define `super' function in each parent classes, which is the correct solution to the problem of multiple inheritance. The `super' function is defined in the parent classes at Lines 5 and 11 of Listing \ref{py_inSup:superParentClass}. 

\begin{explanation}[Order of outputs in Listing \ref{py_inSup:superParentClass}]
	In the listing, the class `C' (Line 20) inherits threes classes i.e. `B', `A' and `D'. When, class `C' is instantiated at Line 25, then the `init' functions are inherited by Line 22. Line 22, first inherits the `init' function of `class B', because `B' appears first at Line 20. Hence, line 10 (i.e. `reached B') is printed first. Next, Line 11 uses the `super().\_\_init\_\_', therefore Python interpretor goes to `init' function of next class i.e. A and prints `reached A' from Line 4. Line 5, again uses the `super' function, therefore interpretor goes to next class i.e. `D' and prints the `reached D' from Line 16. Then again, `super' function is used, but this time there is no other class to inherit, therefore interpretor goes to the line 18 and prints `D' and returns to the `super' of previous class (i.e. Line 5) and prints the next Line i.e. `A'. Next, it goes to `super' of class `B' (i.e. Line 12) and finally it prints the Line 13 i.e. `B'. 
	
	\begin{noNumBox}
	\textbf{Remember that, the statements which appear after the `super' functions, the print orders are opposite to the order of inheritances (see outputs at Lines 29-31); whereas the statements which appear before the super, have the same sequences as the inheritance sequences (see Line 26-28).} 
	\end{noNumBox}
\end{explanation}

\lstinputlisting[
caption    = {`super' in parent class for multiple inheritance},
language = Python,
label      = {py_inSup:superParentClass}
]{superParentClass.py}

\section{Math problem} \label{MathProblem}
This section summarizes the above examples using a mathematical problem. Here we want to calculate $x*2+5$ where $x = 3$. In case 1 and case 2, \_\_init\_\_ function is invoked directly by calling it using class name, which is the wrong way to solve the problem as discussed in Section \ref{inheritMultipleClass}.

\subsection{Case 1: Wrong method}
Listing \ref{py_inSup:mathWrong1} gives the correct answer to the problem, but the answer depends on the order of invoking methods (i.e. Line 13 and 14) rather that order of inheritance (i.e. Line 10). e.g. if we interchange the line 14 and 13, the answer will be changed as shown in next section. 
\lstinputlisting[
caption    = {Answer depends on order of `invoking' the class rather the inheritance},
language = Python,
label      = {py_inSup:mathWrong1}
]{mathWrong1.py}

\subsection{Case 2: Problem with case 1}
In Listing \ref{py_inSup:mathWrong2}, line 13 and 14 of Listing \ref {py_inSup:mathWrong1} are interchanged, therefore the solution is also changed as shown in Line 17. Since, lines are interchanged, therefore `plus' operations is performed first, and then multiplication is performed. 

\lstinputlisting[
caption    = {Problem with Listing \ref{py_inSup:mathWrong1}},
language = Python,
label      = {py_inSup:mathWrong2}
]{mathWrong2.py}

\subsection{Case 3: Correct method}
Here, `super' method is used as discussed in Section \ref{superCorrectSolution}. Now, solution depends on the order of inheritance, which is easier to manage than `Case 1'. 

\lstinputlisting[
caption    = {Answer depends on the order of inheritance},
language = Python,
label      = {py_inSup:mathCorrect}
]{mathCorrect.py}

\section{Different parameters in \_\_init\_\_}
In all the above examples, \_\_init\_\_ has only one parameter i.e. ``self''. If classes have different parameters in \_\_init\_\_ functions, then above examples will not work. For different parameters in inheriting classes, we need to modify our code so that multiple inheritance work properly.
In Listing \ref{py_inSup:multipleInitParameter}, **kwargs is used to solve the problem. Note that, **kwargs is used in all the `\_\_init\_\_' methods i.e. Lines 3, 9 and 1, because the `init' functions have different parameters at these lines. Also, **kwargs is used with super() functions i.e. at Lines 6, 12 and 17. In this way, problem of multiple parameters in `init' functions can be solved.

\lstinputlisting[
caption    = {Different parameters in \_\_init\_\_},
language = Python,
label      = {py_inSup:multipleInitParameter}
]{multipleInitParameter.py}

\section{Conclusion}
In this chapter, we saw the use of `super' function for various types of inheritance situations. Also, we discuss the multiple inheritance with different parameters in `init' functions. 