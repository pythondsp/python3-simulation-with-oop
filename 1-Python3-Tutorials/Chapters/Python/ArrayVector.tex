\section{Numpy, Scipy and Matplotlib}

In this section, we will use various python libraries, i.e. \textbf{Numpy}, \textbf{Scipy} and \textbf{Matplotlib}, which are very useful for scientific computation. With Numpy library, we can define array and matrices easily. Also, it contains various useful mathematical function e.g. random number generator i.e. `rand' and `randn' etc. Matplotlib is used for plotting the data in various format. Some of the function of these libraries are shown in this section. Further, Scipy library can be used for more advance features e.g. complementary error function (erfc) and LU factorization etc.  

\begin{pyExmp}
	Listing \ref{py_ch1:numpyMatplot} generates the sine wave using Numpy library; whereas the Matplotlib library is used for plotting the data. 
\end{pyExmp}

\lstinputlisting[
caption    = {Sine wave using Numpy and Matplotlib, Fig. \ref{fig:numpyMatplot}},
label      = {py_ch1:numpyMatplot}
]{numpyMatplot.py}


\begin{explanation}[Listing \ref{py_ch1:numpyMatplot}]
	First line import numpy library to the code. Also, it is imported with shortname `np'; which is used in line 5 as `np.linspace'. If line 2 is written as `import numpy', then line 5 should be written  as `numpy.linspace'. Further, third line import `pyplot' function of `matplotlib' library as plt. Rest of the lines are explained as comments in the listing. 
\end{explanation}

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{SineWave.pdf}
	\caption{Sine wave using Numpy and Matplotlib, Listing \ref{py_ch1:numpyMatplot}}
	\label{fig:numpyMatplot}
\end{figure}

\subsection{Arrays}

Arrays can be created using `arange' and `array' commands as shown below, 

\subsubsection{arange}
One dimensional array can be created using `arange' option as shown below, 

\lstinputlisting[
caption    = {arange},
label      = {py_ch1:arangeEx}
]{arangeEx.py}


\subsubsection{array}
Multidimensional array can not be created by `arange'. Also, `arange' can only generate sequences and can not take user-defined data. These two problems can be solved by using `array' option as shown in Listing \ref{py_ch1:arrayEx},  

\lstinputlisting[
caption    = {array},
label      = {py_ch1:arrayEx}
]{arrayEx.py}

\subsection{Matrix}
Similar to array, we can define matrix using `mat' function of numpy library as shown in Listing \ref{py_ch1:matrixEx}. Also, LU factorization of the matrix is shown in Listing \ref{py_ch1:scipyEx} using Scipy library. There are differences in results of mathematical operations on the matrices defined by `array' and `mat', as shown in the Listing \ref{py_ch1:scipyEx}; e.g. `dot' function is required for matrix multiplication of array; whereas `*' sign performs matrix multiplication for `mat' function. 

\lstinputlisting[
caption    = {Matrix},
label      = {py_ch1:matrixEx}
]{matrixEx.py}


\lstinputlisting[
caption    = {LU Factorization of Matrix},
label      = {py_ch1:scipyEx}
]{scipyEx.py}


\section{SPYDER} \label{sec:SPYDER}
Saving simulation results using python code can be quite complicated. Further, reading data from those files is even more complicated as shown in Appendix \ref{app:SaveResultWithoutSpyder}. 

This problem can be easily solved by using SPYDER environment. SPYDER can save the data in various formats, but `.spydata' is the preferred format. Further, the results can be saved in `.mat' format as well, which can be used by MATLAB software.

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{SPYDER.PNG}
	\caption{SPYDER Environment}
	\label{fig:SPYDER}
\end{figure}

Fig. \ref{fig:SPYDER} shows the SPYDER window. On the left side of the window, actual code is shown. From the view option (at the top), you can add more toolbars and panes to your window. `Import Data' and `Save data as' buttons (on the right side) are used to save and read the data as shown next.

\begin{noNumBox}
	We can write/run all the codes in previous sections using SPYDER environment. Further, it has auto-completion option as well; e.g. if we write `import numpy as np', then from next line, after writing `np.', it will display all the possible commands available in the library. 
\end{noNumBox}

\subsection{Python/IPython Console}
 Note that, if you want to publish the results in research journals, then editors may ask for editable figure files e.g. .ps or .eps formats; often, they do not accept .jpg or .PNG formats. Python console, by default, generates figure in separate window outside the console; and then figure can be saved in various formats e.g. PDF, .eps and .ps etc. Also, we can change the plot style before saving the plots. Whereas, if you try to plot the figure using IPython console, it will generate the figure inside the console which can be saved in .PNG format only. We can change this behavior from \textbf{Tools$\rightarrow$Preferences$\rightarrow$IPython Console$\rightarrow$Graphics backends$\rightarrow$QT}, and restarting the console. After this setting, figure will be displayed in separate window.  

To open a `Python console', click on `Consoles $\rightarrow$ Open a Python console' at the top of SPYPER window. It will open the console as shown in Fig. \ref{fig:Console} where console is open with name `Python 2'. You may get different number there. 

\begin{noNumBox}
	Note that, console window may contain various Python/IPython console simultaneously; but the console which appear on that window is the active console e.g. in  Fig. \ref{fig:Console}, `Python 2' is active console. 
\end{noNumBox}


\begin{figure}
	\centering
	\includegraphics[width=0.4\textwidth]{Console.png}
	\caption{Python Console}
	\label{fig:Console}
\end{figure}

\subsection{Save Data}

First open the code in Listing \ref{py_ch1:fileSaveEx} in SPYDER environment as shown in Fig. \ref{fig:SPYDER}; and then press the `Run' button. The code will execute and all the variables will be displayed in the `variable explorer' e.g. SNR\_db etc. as shown in Fig. \ref{fig:variablePane}. Now click on the `save data as' button to save it as .spydata file with name `BPSK', as shown in Fig. \ref{fig:saveSpydata}. 

\begin{noNumBox}
	Although, SPYDER can save data in `.mat' file as well, but it creates problems  while reading it. Therefore, it is better to save data in `.spydata' file.
\end{noNumBox}

\begin{figure}[!h]
	\centering
	\begin{subfigure}[t]{0.6\textwidth}
		\centering
		\includegraphics[scale=0.6]{variablePane.PNG}
		\caption{Data generated after running code}
		\label{fig:variablePane}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[scale=0.5]{saveSpydata.png}
		\caption{Save data in .spydata file}
		\label{fig:saveSpydata}
	\end{subfigure}
	\caption{Run code and save data in .spydata file}
\end{figure}

\lstinputlisting[
caption    = {BER of BPSK system and Random Numbers},
label      = {py_ch1:fileSaveEx}
]{fileSaveEx.py}

\begin{explanation}[Listing \ref{py_ch1:fileSaveEx}]
	Line 11 in Listing \ref{py_ch1:fileSaveEx}, is the implementation of following equation for different SNR values,
	\begin{equation}
		theoryBER = \frac{1}{2}erfc\left( {\sqrt {SNR} } \right)
	\end{equation} 
	Further, line 14 and 15 generates random variables with different distribution. 
\end{explanation}

\subsection{Read Data} \label{sec:readdata}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\textwidth]{readData.PNG}
	\caption{Load the BPSK.spyderdata file before running this code}
	\label{fig:readData}
\end{figure}

First open the code in Listing \ref{py_ch1:fileReadEx} as shown in Fig. \ref{fig:readData}. After that click on the `import data' button on variable-explorer and open the `BPSK.spydata' file, which we saved in previous section. Variable-explorer will again display the stored data as in Fig \ref{fig:variablePane}. Now press the `Run' button again, and Listing \ref{py_ch1:fileReadEx} will execute and plot the data stored in .spydata file as shown in Fig. \ref{fig:plotData}.

\lstinputlisting[
caption    = {Code for plotting the saved data},
label      = {py_ch1:fileReadEx}
]{fileReadEx.py}

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[scale=0.35]{BERPlot.pdf}
		\caption{theoryBER: BPSK}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[scale=0.35]{RandomPlot.pdf}
		\caption{rn, r: Random numbers}
	\end{subfigure}
	\caption{Plots from the data stored in BPSK.spydata}
	\label{fig:plotData}
\end{figure}

