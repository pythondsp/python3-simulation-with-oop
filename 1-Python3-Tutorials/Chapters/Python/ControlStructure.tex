\section{Control structure}
In this section, various simple python codes are shown to explain the control structures  available in python.

\subsection{if-else}
\textit{$If-else$} statements are used to define different actions for different conditions. Symbols for such conditions are given in table \ref{tbl:conditionSymbol}, which can be used as shown in Listing \ref{py_ch1:ifOnly}. Three examples are shown in this section for three types of statements i.e. \textit{if}, \textit{if-else} and \textit{if-elif-else}.

\begin{table}
	\centering
	\includegraphics{conditionSymbol}
	\caption{Symbols for conditions}
	\label{tbl:conditionSymbol}
\end{table}

\subsubsection{If statement}
In this section, only if statement is used to check the even and odd number. 
\begin{pyExmp}[Single If statement]
	Listing \ref{py_ch1:ifEx} checks whether the number stored in variable $x$ is even or not.
\end{pyExmp}

\lstinputlisting[
caption    = {If statement},
label      = {py_ch1:ifEx}
]{ifEx.py}

\begin{explanation}[Listing \ref{py_ch1:ifEx}]
	An if statement is made up of the `if' keyword, followed by the condition and colon (:) at the end, as shown in line 4. Also, the line 6 is indented which means it will execute only if the condition in line 4 is satisfied (see Listing \ref{py_ch1:ifelseNested} for better understanding of indentation). Last print statement has no indentation, therefore it is not the part of the `if' statement and will execute all the time. You can check it by changing the value of $x$ to 3 in line 2. Three quotes (in line 11 and 14) are used to comment more than one lines in the code, e.g. $''' results { \ }here '''$ is used at the end of the code (see line 11-20) which contain the results of the code.
	
\end{explanation}

\begin{pyExmp}[Multiple If statements]
Listing \ref{py_ch1:ifOnly} checks the \textbf{even and odd} numbers using $if$ statements. Since there are two conditions (even and odd), therefore two $if$ statements are used to check the conditions as shown in Listing \ref{py_ch1:ifOnly}. Finally output is shown as the comments at the end of the code.  
\end{pyExmp}	

	
	\lstinputlisting[
	caption    = {Multiple If statements},
	label      = {py_ch1:ifOnly}
	]{ifOnly.py}
	
\begin{explanation}[Listing \ref{py_ch1:ifOnly}]
		This code demonstrate that one can use multiple $if$ conditions in codes. In this code, value of $x$ is taken from using line 5 in the code. `int' is used in this line because `input' command takes the value as string and it should be changed to integer value for mathematical operations on it. 
\end{explanation}

\subsubsection{If-else}
As we know that a number can not be even and odd at the same time. In such cases we can use $if-else$ statement. 
 
\begin{pyExmp} \label{ex:ifelse1}
Code in Listing \ref{py_ch1:ifOnly} can be written using If-else statement as show in Listing \ref{py_ch1:ifelse1}.
\end{pyExmp}

\lstinputlisting[
  caption    = {If-else statement},
  label      = {py_ch1:ifelse1}
]{ifelse1.py}

\begin{explanation}[Listing \ref{py_ch1:ifelse1}]
	Line 4 takes the input from the user. After that remainder is checked in line 7. If condition is true i.e. remainder is zero, then line 8 will be executed; otherwise print command inside `else' statement (line 10) will be executed.  
\end{explanation}

\subsubsection{If-elif-else}

In previous case, there were two contions which are implemented using if-else statement. If there are more than two conditions then `elif' block can be used as shown in next example. Further, `If-elif-else' block can contain any number of `elif' blocks between one `if' and one `else' block. 

\begin{pyExmp}
Listing \ref{py_ch1:ifelseNested} checks whether the number is divisible by 2 and 3, using nested $if-else$ statement. 
\end{pyExmp}

\lstinputlisting[
  caption    = {If-elif-else statement},
  label      = {py_ch1:ifelseNested}
]{elifEx.py}

\begin{explanation}[Listing \ref{py_ch1:ifelseNested}]
Let's discuss the indentation first. First look at the indentation at lines $7$ and $8$. Since line $8$ is shifted by one indentation after line $7$, therefore it belongs to line $7$, which represents that python-interpreter will go to line $8$ only if line $7$ is true. Similarly, print statements at line $11$ and $12$ are indented with respect to line $10$, therefore both the print statement will be executed when python-interpreter reaches to $else$ condition.  

Now we will see the output for $x=12$. For $x=12$,  $if$ statement is satisfied at line $7$, therefore python-interpreter will go to line $8$, where the divisibility of the number is checked with number $3$. The number is divisible by $3$ also, hence corresponding print statement is executed as shown in line $24$. After this, python-interpreter will exit from the $if-else$ statements and reached to line $19$ and output at line $25$ will be printed. 

Lets consider the input $x=7$. In this case number is not divisible by $2$ and $3$. Therefore python-interpreter will reached to line $15$. Since lines $16$, $17$ and $18$ are indented with respect to line $15$, hence all the three line will be printed as shown in line $35-37$ . Finally, python-interpreter will reach to line $19$ and print this line also. 

Lastly, use 15 as the input number. Since it is not divided by 2, it will go to elif statement and corresponding print statement will be executed. 

Since there are three conditions in this example. therefore $elif$ statement is used. Remember that $if-else$ can contain only one $if$ and one $else$ statement , but there is no such restriction of $elif$ statement. Hence, if there higher number of conditions, then we can increase the number of $elif$ statement.
\end{explanation}

Listing \ref{py_ch1:ifelseNested} can be written as Listing \ref{py_ch1:ifelseNested2} and \ref{py_ch1:ifelseNested3} as well. Here  `or' and `and' keywords are used to verify the conditions. The `and' keyword considers the statement as true, if and only if, all the conditions are true in the statement; whereas `or' keyword considers the statement as true, if any of the conditions are true in the statement.

\lstinputlisting[
caption    = {`and' logic},
label      = {py_ch1:ifelseNested2}
]{andLogic.py}

\lstinputlisting[
caption    = {`and' and `or' logic},
label      = {py_ch1:ifelseNested3}
]{orLogic.py}

\subsection{While loop}

$While$ loop is used for recursive action, and the loop repeat itself until a certain condition is satisfied. 

\begin{pyExmp}
Listing \ref{py_ch1:WhileExample1} uses $while$ loop to print numbers 1 to 5. For printing numbers upto 5, value of initial number should be increased by 1 at each iteration, as shown in line 7. 
\end{pyExmp}

\lstinputlisting[
  caption    = {While loop},
  label      = {py_ch1:WhileExample1}
]{WhileExample1.py}

\begin{explanation}[Listing \ref{py_ch1:WhileExample1}]
In the code, line $3$ sets the initial value of the number i.e. $n=1$. Line $5$ indicates that $while$ loop will be executed until $n$ is less than 6. Next two lines i.e. line $6$ and $7$, are indented with respect to line $5$. Hence these line will be executed if and only if the condition at line $5$ is satisfied. 

Since the value of $n$ is one therefore while loop be executed. First, number 1 is printed by line $6$, then value of $n$ is incremented by 1 i.e. $n=2$. $n$ is still less than 6, therefore loop will be executed again. In this iteration value of $n$ will become 3, which is still less than 6. In the same manner, loop will continue to increase the value of $n$ until $n=6$. When $n=6$, then loop condition at line $5$ will not be satisfied and loop will not be executed this time. At this point python-interpreter will reach to line $8$, where it will print the value stored in variable $n$ i.e. 6 as shown in line $14$. 

Also, look at $print$ commands at lines $3, 6$ and $8$. At line $6$, $end = '' { \ } ''$ is placed at the end, which results in no line change while printing outputs as shown at line $13$. At line $8$, $\backslash n$ is used to change the line, otherwise this print output will be continued with output of line $6$. 
\end{explanation}

\subsection{For loop}
Repetitive structure can also be implemented using $for$ loop. \textbf{For} loop requires the keyword \textbf{in} and some \textbf{sequence} for execution. Lets discuss the \textbf{range} command to generate sequences, then we will look at $for$ loop. Some outputs of $range$ commands are shown in Listing \ref{py_ch1:RangeCommand}.

\lstinputlisting[
  caption    = {Range command},
  label      = {py_ch1:RangeCommand}
]{RangeCommand.py}


\begin{explanation}[Listing \ref{py_ch1:RangeCommand}]
From the outputs of $range$ commands in the listing, it is clear that it generates sequences of integers. Python indexing starts from zero, therefore command $range(5)$ at line $3$ generates five numbers ranging from $0$ to $4$. 

At line $6$, two arguments are given in $range$ commands i.e. $1$ and $4$. Note that output for this at line $7$ starts from $1$ and ends at $3$ i.e. last number is not included by Python in the output list.

At line $9$ and $12$, three arguments are provided to $range$ function. In these cases, third argument is the increment value e.g. line $12$ indicates that ``number should start from $15$ and stop at number $7$ with a decrement of $2$ at each step. Note that last value i.e. $7$ is not included in the output again.  Similarly, output of line $9$ does not include $19$. 
\end{explanation}

\begin{pyExmp}
Listing \ref{py_ch1:ForExample1} prints numbers from $1$ to $5$ in forward and reverse direction using range command.
\end{pyExmp}

\lstinputlisting[
  caption    = {For loop},
  label      = {py_ch1:ForExample1}
]{ForExample1.py}


\begin{explanation}[Listing \ref{py_ch1:ForExample1}]
At line $4$, command $range(5)$ generates the five numbers, therefore loop repeats itself five times.  Since, output of range starts from $0$, therefore $i$ is incremented by one before printing. Line $6$ shows that the variable $i$ stores only one value at a time, and the last stored value is $4$ i.e. last value of $range(5)$.  

At line $9$, variable $j$ is used instead of $i$ and range command generates the number from 1 to 5 again but in reverse order.  Note that number of iteration in for loop depends on the number of elements i.e. length of the $range$ command's output and independent of the element values. Line $10$ prints the current value of $j$ at each iteration. Finally, line $15$ prints the last value stores in variable $j$ i.e. $j=1$, which is the last value generated by command $range(5,0,-1)$.  

Code in line $24$ shows that, how the values are assigned to the iterating variable `i' from a list. The list `fruits' contains three items, therefore loop will execute three times; and different elements of the list are assigned to variable `i' at each iteration i.e. apple is assign first, then banana and lastly orange will be assigned. 
\end{explanation}


\section{Function}\label{sec:functionsPython}
Some logics may be used frequently in the code, which can be written in the form of functions. Then, the functions can be called whenever required, instead of rewriting the logic again and again.

\begin{pyExmp}
In Listing \ref{py_ch1:funcEx}, the function `addTwoNum' adds two numbers. 
\end{pyExmp} 

\lstinputlisting[
caption    = {Function},
label      = {py_ch1:funcEx}
]{funcEx.py}

\begin{explanation}[Listing \ref{py_ch1:funcEx}]
	In python, function is defined using keyword `def'. Line 2 defines the function with name `addTwoNum' which takes two parameter i.e. a and b. Line 3 add the values of `a' and `b' and stores the result in variable `sum'. Finally line 4 returns the value to function call which is done at line 6. 
	
	In line 6, function `addTwoNum' is called with two values `4' and `5' which are assigned to variable `a' an `b' respectively in line 2. Also, function returns the `sum' variable from line 4, which is stored in variable `results' in line 6 (as line 6 called the function). Finally, line 7 prints the result.
\end{explanation}

\begin{pyExmp}
	In Listing \ref{py_ch1:funcEx2}, the function is defined with some default values; which means if user does not provide all the arguments' values, then default value will be used for the execution of the function. 
\end{pyExmp} 

\lstinputlisting[
caption    = {Function with default arguments},
label      = {py_ch1:funcEx2}
]{funcEx2.py}

\begin{explanation}[Listing \ref{py_ch1:funcEx2}]
	Function of this listing is same as Listing \ref{py_ch1:funcEx}. Only difference is that the line 3 contains a default value for num2 i.e. `num2 = 2'. Default value indicates that, if function is called without giving the second value then it will be set to 2 by default, as shown in line 6. Line 6 pass only one value i.e. 3, therefore num1 will be assign 3, whereas num2 will be assigned default value i.e. 2. Rest of the the working is same as Listing \ref{py_ch1:funcEx}.  
\end{explanation}


\begin{noNumBox}
	There are various other important python features e.g. classes, decorators and descriptors etc. which are not explained here as we are not going to use these in the coding. Further, using these features we can make code more efficient and reusable along with less error-prone. 
\end{noNumBox}

