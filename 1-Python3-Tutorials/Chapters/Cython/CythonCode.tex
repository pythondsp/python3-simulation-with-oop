\section{Cython}

Cython is a programming language which translate the Python code to optimized C/C++ code. Due to optimized code, there is an increase in the simulation speed, as shown in Table \ref{tbl:timeComparision}. This table compares the performances of various codes, which are used in this tutorial. 


\begin{table}[!h]
	\centering
	\caption{Speed comparison of various codes described in section \ref{sec:timecomparision}}
	\includegraphics[scale=0.9]{timeComparision}
	\label{tbl:timeComparision}
\end{table}

\subsection{First Cython Code (.pyx file)}\label{sec:firstCython}
Cython files are saved as `.pyx' files. Further, Cython supports all the python syntax, therefore simply by changing the filetype (i.e. .py to .pyx) we can achieved faster speed of simulation, as compared in Row 1 and 2 of Table \ref{tbl:timeComparision}. Following are the two steps to convert the python codes in Listing \ref{py_cython:funcFile} and \ref{py_cython:funcMain} to cython codes, 

\begin{enumerate}
	\item \textbf{Change File type}: \\
	Save Listing \ref{py_cython:funcFile} as `.pyx' file with some other name, e.g. `funcCfile.pyx' as shown in Listing \ref{py_cython:funcCfile}. Here, different file names are chosen to avoid confusion with file names. 
	
	\lstinputlisting[
	caption    = {Save definitions in separate file},
	language = Python,
	label      = {py_cython:funcCfile}
	]{funcCfile.pyx}
	
	
	\item \textbf{Import `.pyx' file to main file}: \\
	The `import; command in python does not search for .pyx file. We need to add two lines,  (i.e. lines 4-5 in in Listing \ref{py_cython:funcCmain}), to tell python interpreter to find files in `.pyx' format as well. 
	\\
	\\
	Above two steps convert the python-code into cython-code. Now, we can run the cython code using ``python funcCmain.py'' command to see the outputs, as shown in Listing \ref{py_cython:funcCmain}. 
	
	
	\lstinputlisting[
	caption    = {Call definition to the main code},
	language = Python,
	label      = {py_cython:funcCmain}
	]{funcCmain.py}
	
	\begin{noNumBox}
	There are three methods available to import the cython code in the file. 
	\begin{enumerate}
		\item 	One method is discussed in this section, which is sufficient as long as we are writing our code in Python and Cython language.
		\item Second method is used in Jupyter Notebook, which is described on the \href{http://pythondsp.blogspot.com/2016/10/speed-up-simulations.html}{website}. Also, you can download the copy of Jupyter notebook for this tutorial from the \href{http://pythondsp.blogspot.com/2016/10/speed-up-simulations.html}{homepage}.
		\item Third method needs a setup.py file, which is required when we are using `C/C++' codes along with the python codes. Since, we are interested only in Python-Codes for simulations, therefore it is not discussed in the tutorial. 
	\end{enumerate}	
	\end{noNumBox}
\end{enumerate}

\subsection{def, cdef and cpdef}
Cython definition can be created in three ways i.g. using `def', `cdef' and `cpdef' keywords. These three keywords are discussed in this section, 

\subsubsection{def}
Cython `def' can be defined in two ways as shown as shown at lines 4 and 9 in Listing \ref{py_cython:cythonDef}.  In first method i.e. line 4, normal python definition is written but saved in `.pyx' file, as discussed in section \ref{sec:firstCython}. In the second method, we define argument type in `def' (see `int x' at line 9  in Listing \ref{py_cython:cythonDef}). Note that we can not define the return type for the function using `def'. For this we use `cdef' and `cpdef'. 

\subsubsection{cdef}
We can define the return type of the function using `cdef' as shown at line 15 in Listing \ref{py_cython:cythonDef}. Here first `int' is the return type of the definition. 
\begin{noNumBox}
	`cdef' can be called from the same file only e.g. in Listing \ref{py_cython:cythonDef}, line 25 is calling `cdef' function (i.e. cfunc) at line 15. But this function can not be called from other files e.g. line 15 in Listing \ref{py_cython:cythonDefMain} will generate error. 
\end{noNumBox}

\subsubsection{cpdef}
Cython function with return type, can be called from outside file if it is define as `cpdef'. `cpdef' adds the python wrapper to the definition, therefore it can be called from outside files e.g. line 18 in Listing  \ref{py_cython:cythonDefMain} is calling `cpdef' function at line 20 in Listing  \ref{py_cython:cythonDef}. Further, line 21 in Listing  \ref{py_cython:cythonDefMain} is calling `cpdef' function at line 24 in Listing  \ref{py_cython:cythonDef}; then line 24 is calling `cdef' definition at line 15 (which can not be called directly from outside files). 

\lstinputlisting[
caption    = {Cython Definitions},
language = Python,
label      = {py_cython:cythonDef}
]{cythonDef.pyx}

\lstinputlisting[
caption    = {Main function to call Cython Definitions},
language = Python,
label      = {py_cython:cythonDefMain}
]{cythonDefMain.py}

\begin{noNumBox}
Note that, adding `argument types', `return types' and `variable types (see Line 28-29 in Listing \ref{py_cython:cythonFunc})' increase the speed of the code significantly as shown in Table \ref{tbl:timeComparision}. To convert a python code to cython code, look for the variables, which do not change their type during execution and define their types as show in Listing \ref{py_cython:cythonFunc}. 
\end{noNumBox}

\section{Timing Comparison: Python, Cython, Numba}\label{sec:timecomparision}
In this section, simulation times are compared for different codes, to execute the simple loops. Further, Numba compiler is discussed which can increase the speed of the code significantly by adding decorator to each definition.

\subsection{Python}
In Listing \ref{py_cython:purePythonLoop}, two `for' loops are defined and value of variable `x' is increasing at each iteration. Execution time for this simple loop is calculated for different codes. Further, `time' function is used to measure the simulation time as shown in line 3 of Listing \ref{py_cython:purePythonLoopMain}. 

\lstinputlisting[
caption    = {Pure python loop},
language = Python,
label      = {py_cython:purePythonLoop}
]{purePythonLoop.py}

\lstinputlisting[
caption    = {Main function to call purePythonLoop.py},
language = Python,
label      = {py_cython:purePythonLoopMain}
]{purePythonLoopMain.py}

\subsection{Cython}
In Listing \ref{py_cython:cythonFunc}, cython functions are defined in four ways as shown below,
\begin{enumerate}
	\item pyloop: this is simple python loop saved in `.pyx' file
	\item cyloop: in this function, python function (def) with `argument type' is defined. 
	\item cypyloop: Here `return type' and `argument type' are defined using `cpdef'. 
	\item cypyloop2: In this function `return type', `argument type' and `variable type' are defined using `cpdef'. 
\end{enumerate}
  In `cypyloop2' function (line 28 in Listing \ref{py_cython:cythonFunc}), data types of i and j are defined; which increases the simulation speed by 350 times as shown in Table \ref{tbl:timeComparision}. Run Listing \ref{py_cython:cythonMain} to compare the simulation times of various codes.  

\lstinputlisting[
caption    = {Main function to call Cython Definitions},
language = Python,
label      = {py_cython:cythonFunc}
]{cythonFunc.pyx}

\lstinputlisting[
caption    = {Main function to call cythonFunc.py},
language = Python,
label      = {py_cython:cythonMain}
]{cythonMain.py}

\subsection{Numba}
Numba can increase the speed of the python code just by adding one line before each definition as shown in Listing \ref{py_cython:numbaLoop}. In line 3 of the listing, `autojit' is imported from Numba library, then a decorator is placed before the definition at line 7. After these changes, run the code in Listing \ref{py_cython:numbaMain}; this time code will run 100 times faster than the original python code as shown in Table \ref{tbl:timeComparision}. Lastly, there are various other options available for Numba-decorators, but `autojit' works fine for most of the cases. 

\lstinputlisting[
caption    = {Add Numba to python definitions},
language = Python,
label      = {py_cython:numbaLoop}
]{numbaLoop.py}

\lstinputlisting[
caption    = {Main function to call numbaLoop.py},
language = Python,
label      = {py_cython:numbaMain}
]{numbaMain.py}

