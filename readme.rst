About
^^^^^

In this tutorial, various Python examples (along with OOPs concepts) are discussed for Mathematical simulations. Further, Matplotlib, Numpy, Scipy, Spyder, Cython and Numba are  discussed to create the simulator effectively. Lastly, simulation speed of Cython, Numba and Matlab are also compared. 


The tutorial is available at following link, 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PDF   : https://drive.google.com/file/d/0B9zymgX8PsIXNUlHM2Z1T1A2NEE/

CODES : https://drive.google.com/file/d/0B9zymgX8PsIXRV9zVGVaTWdVZnc/


See below link, for complete list of tutorials,
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

http://pythondsp.readthedocs.io/en/latest/pythondsp/toc.html


Below is the list of chapters in this tutorial, 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Python for simulation
2. Plotting data with Matplotlib
3. Speed up the simulations
4. Confirm theory with simulation
5. Iterating over the list
6. Object oriented programming
7. Simulation using OOP
8. Inheritance : super()
9. Function, decorator, @property and descriptor 
10. More on methods
11. Appendix

     a) Saving results without SPYDER
     b) Relative imports in Python
     c) Numba, Cython and Matlab codes
